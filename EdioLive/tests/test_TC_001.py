import datetime
import os
import time

import allure
from selenium import webdriver
import pytest

from EdioLive.pageObjects.CoursePage import CoursePage
from EdioLive.pageObjects.DashboardPage import DashboardPage
from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData




class Test(BaseClass):


    def test_test4(self):
        global teacherHandle, studentHandle, jit, jit2, driver, cp2, dash2, TeacherHandleDefault, defaultstudentHandle

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        print(TestData.CHROME_PATH)
        #driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)
     #   driver = EventFiringWebDriver(driver_chrome, MyListener())
        driver.get(TestData.URL)
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)
        jit = Jitsi(driver)

        #dash = login.enterLoginCredentials()
        dash = login.enter_credentials(TestData.TEACHER_USERNAME1,TestData.TEACHER_PASSWORD)
        TeacherHandleDefault = dash.enter_classroom1()
        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()
        jit.validate_default_state_of_teacher()
        jit.validate_visibility_Of_sidePanel_elements_teacher()

    ###################################################################################

    #Student enters the session and requests for mic access
    # and Teacher allows the mic access. Student should be able to access mic


    @pytest.mark.regression
    @pytest.mark.depends(on=['test_test4'])
    def test_test5(self):

        global cp3, driver2, cp4
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        #driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH, options=chrome_options)
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)

        #  driver2 = EventFiringWebDriver(driver_chrome2, MyListener())
        driver2.get(TestData.URL)
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        jit2 = Jitsi(driver2)
        cp3 = CoursePage(driver2)
      #  cp4 = CoursePage(driver)

        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1,TestData.STUDENT_PASSWORD1)
        jit2=dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        jit2.validate_default_state_of_student()
        jit2.request_mic_access()

        studentHandle = dash2.getDefaultWindowHandle()
        print("Student :" + studentHandle)

        # Switching to teacher screen
        driver.switch_to.window(teacherHandle)
        jit.click_allow_button()
        driver2.switch_to.window(studentHandle)
        jit2.switchIframe()
        jit2.click_ok_button()
        jit2.click_mic_button_Student()

        #jit2.click_exit_button()

        time.sleep(5)
        driver2.quit()
        #driver2.switch_to.window(defaultstudentHandle)

    #################################################################################

    # Student enters the session and requests for mic access
    # and Teacher rejects the mic access. Student should not be able to access mic

    
    @pytest.mark.depends(on=['test_test5'])
    def test_test6(self):
        global cp3, driver2, cp4
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        # driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH, options=chrome_options)
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH, options=chrome_options)

        #  driver2 = EventFiringWebDriver(driver_chrome2, MyListener())
        driver2.get(TestData.URL)
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        jit2 = Jitsi(driver2)
        cp3 = CoursePage(driver2)
        #  cp4 = CoursePage(driver)

        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()


        jit2.validate_default_state_of_student()
        jit2.request_mic_access()

        studentHandle = dash2.getDefaultWindowHandle()
        print("Student :" + studentHandle)
        # Switching to teacher screen
        driver.switch_to.window(teacherHandle)
        jit.reject_mic_access()
        #jit.clickMic()

        driver2.switch_to.window(studentHandle)
        jit2.switchIframe()
       # jit3.validate_default_state_of_student()

        #################################################################
        #jit3.click_exit_button()

    @pytest.mark.depends(on=['test_test2'])
    def test_test7(self):
        driver2.close()
        driver2.quit()
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        #jit.click_exit_button()
        driver.close()
        driver.quit()


        #################################################################

