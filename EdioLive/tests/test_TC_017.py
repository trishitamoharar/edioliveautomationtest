import time

import allure
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pytest


from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class Test(BaseClass):

    # test_chat_notification_2

    def test_test71(self):
        global teacherHandle, jit,jit2,jit3, driver,driver2,driver3, cp2, dash2, TeacherHandleDefault, defaultstudentHandle,studentHandle2,studentHandle1

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                  options=chrome_options)
        #   driver = EventFiringWebDriver(driver_chrome, MyListener())

        driver.get("https://demo-edio.ccaeducate.me/")
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)

        dash = login.enter_credentials(TestData.TEACHER_USERNAME1, TestData.TEACHER_PASSWORD)
        jit = dash.enter_classroom1()
        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()
      #  jit.validate_default_state_of_teacher()
       # jit.validate_visibility_Of_sidePanel_elements_teacher()

        ###################################################################################

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                   options=chrome_options)

        driver2.get("https://demo-edio.ccaeducate.me/")
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        studentHandle1=dash2.getDefaultWindowHandle()
       # jit2.validate_default_state_of_student()
        #jit2.click_ok_button()
    ###################################################################################

    # Validate if the notification count is visible to student if the and the focus
    # is on PARTICIPANTS tab
    def test_test72(self):
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.click_settings_button()
        jit.enable_chat_forAll()
        jit.click_exit_button_Settings()
        jit.click_chat_button()
        jit.sendChatMessages("A")
        jit.sendChatMessages("B")
        jit.sendChatMessages("C")
        jit.sendChatMessages("D")
        jit.sendChatMessages("E")
        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        jit2.click_ok_button()
        jit2.validate_chat_notification_count(5)
        jit2.click_chat_button()

    # Validate if the notification count is visible to student if the side panel is closed
    # and the focus is on PARTICIPANTS tab
    def test_test73(self):
        jit2.click_participant_button()
        jit2.click_close_sidePanel_Button()
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
       # jit.click_chat_button()
        jit.sendChatMessages("A1")
        jit.sendChatMessages("B2")
        jit.sendChatMessages("C3")
        jit.sendChatMessages("D4")
        jit.sendChatMessages("E5")
        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        jit2.validate_sidePanel_notification_Count("5")

    #Validate if the notification count is visible to student if the side panel is closed
    #and the focus is on CHATS tab

    def test_test74(self):
        jit2.click_open_sidePanel()
        jit2.click_chat_button()
        jit2.click_close_sidePanel_Button()
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.sendChatMessages("1")
        jit.sendChatMessages("2")
        jit.sendChatMessages("3")
        jit.sendChatMessages("4")
        jit.sendChatMessages("5")
        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        jit2.validate_sidePanel_notification_Count("5")


    def test_test75(self):
        driver.close()
        driver.quit()
        driver2.switch_to.window(studentHandle1)
        driver2.close()
        driver2.quit()






