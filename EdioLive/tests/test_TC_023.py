import time

from numpy.core.defchararray import strip
from selenium import webdriver
import pytest
from selenium.webdriver import ActionChains

from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class Test(BaseClass):


    def test_test93(self):
        global teacherHandle, jit, jit2, jit3, driver, driver2, driver3, cp2, dash2, TeacherHandleDefault, defaultstudentHandle, studentHandle2, studentHandle1

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                  options=chrome_options)
        #   driver = EventFiringWebDriver(driver_chrome, MyListener())

        log = self.getLogger()
        driver.get("https://demo-edio.ccaeducate.me/")
        log.info("Logged into EDIO")

        driver.maximize_window()
        driver.implicitly_wait(120)
        login = LoginPage(driver)

        dash = login.enter_credentials(TestData.TEACHER_USERNAME1, TestData.TEACHER_PASSWORD)
        log.info("Entered the Valid Credentials")

        jit = dash.enter_classroom1()

        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()
    ###################################################################################

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)

        driver2.get("https://demo-edio.ccaeducate.me/")
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        jit2 = Jitsi(driver2)

        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        studentHandle1 = dash2.getDefaultWindowHandle()

    def test_test94(self):

        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        driver.find_element_by_xpath("//div[@class='button-group-right']/div[1]/div/button").click()
        time.sleep(3)
        #driver.find_element_by_xpath("//label[@labelfor='chatSwitch']/span").click()
        driver.find_element_by_xpath("//label[@labelfor='stdTechChatSwitch']/span").click()
        time.sleep(2)
        ele1 = driver.find_element_by_xpath("//div[@class='Content__DefaultWrapperComponent-sc-1npw367-0 ffdVFW']/div[1]/h4/div")
        driver.execute_script("arguments[0].click();", ele1)



        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        time.sleep(5)
        ele = driver2.find_element_by_xpath("//div[@class='popup_overlay show-notification']/div/div/button")
        driver2.execute_script("arguments[0].click();", ele)
        jit2.click_chat_button()
        driver2.find_element_by_xpath("//div[@class=' css-yk16xz-control']").click()
        time.sleep(4)
        t = driver2.find_element_by_xpath("//div[@class=' css-1w9j89e-menu']/div")
        listval = t.find_elements_by_xpath("./child::*")
        print(listval)
        count = len(listval)
        print(count)
        print("*************")
        val = listval[0].text
        #listval[0].click()

        for i in listval:
            print(i.text)
            if "Voitek, Cayce" in i.text:
                i.click()

        expectedmessagecount = 2
        message1 = "Student message1"
        message2 = "student message2"
        jit2.send_chatMessages(message1)
        jit2.send_chatMessages(message2)



        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        chatcount = int(driver.find_element_by_xpath("//div[@class='participent-tab']/ul/li[1]/a/span[2]").text)
        assert chatcount == 2
        jit.click_chat_button()
        time.sleep(5)
        driver.find_element_by_xpath("//div[@class=' css-yk16xz-control']").click()
        time.sleep(2)
        t = driver.find_element_by_xpath("//div[@class=' css-1w9j89e-menu']/div")
        listval = t.find_elements_by_xpath("./child::*")
        print(listval)
        count = len(listval)
        print(count)
        print("*************")
        assert "ABROMITIS, MACY" in listval[1].text
        for i in listval:
            print(i.text)
            if "ABROMITIS, MACY" in i.text:
                assert "2" in i.text
                i.click()
        jit.validate_chat_notification_count(2)
        message3 = "Teacher message1"
        message4 = "Teacher message2"
        jit.send_chatMessages(message3)
        jit.send_chatMessages(message4)

        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        chatcountstu = int(driver2.find_element_by_xpath("//div[@class='participent-tab']/ul/li[1]/a/span[2]").text)
        assert chatcountstu == 2
        #jit2.click_chat_button()
        driver2.find_element_by_xpath("//div[@class=' css-yk16xz-control']").click()
        time.sleep(4)
        t = driver2.find_element_by_xpath("//div[@class=' css-1w9j89e-menu']/div")
        listval = t.find_elements_by_xpath("./child::*")
        print("*************")
        for i in listval:
            print(i.text)
            if "Voitek, Cayce" in i.text:
                assert "2" in i.text
                i.click()
        jit2.validate_chat_notification_count(2)


    def test_test95(self):
        driver.close()
        driver.quit()
        driver2.switch_to.window(studentHandle1)
        driver2.close()
        driver2.quit()



