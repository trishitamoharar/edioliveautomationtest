import time

import allure
from selenium import webdriver
import pytest

from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class Test(BaseClass):

    # Chat Access to Students


    def test_test12(self):
        global teacherHandle, jit, driver, cp2, dash2, TeacherHandleDefault, defaultstudentHandle

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                  options=chrome_options)
     #   driver = EventFiringWebDriver(driver_chrome, MyListener())


        driver.get("https://demo-edio.ccaeducate.me/")
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)
        jit = Jitsi(driver)

        dash = login.enter_credentials(TestData.TEACHER_USERNAME1, TestData.TEACHER_PASSWORD)
        TeacherHandleDefault = dash.enter_classroom1()
        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()
        jit.validate_default_state_of_teacher()
        jit.validate_visibility_Of_sidePanel_elements_teacher()



    ###################################################################################

    #Student enters the session and Teacher allows chat access to all students.

    def test_test13(self):
        global  driver2, studentHandle, jit2
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                   options=chrome_options)
      #  driver2 = EventFiringWebDriver(driver_chrome2, MyListener())
        driver2.get("https://demo-edio.ccaeducate.me/")
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        jit2 = Jitsi(driver2)

        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        jit2.validate_default_state_of_student()


        studentHandle = dash2.getDefaultWindowHandle()
        print("Student :" + studentHandle)

        # Switching to teacher screen
        driver.switch_to.window(teacherHandle)
        jit.switch_Iframe()
        jit.click_settings_button()
        jit.enable_chat_forAll()
        jit.click_exit_button_Settings()


        driver2.switch_to.window(studentHandle)
        jit2.switchIframe()
        time.sleep(3)
        jit2.click_ok_button()
        jit2.click_chat_button()
        jit2.send_chatMessages("A")
        jit2.send_chatMessages("B")
        jit2.send_chatMessages("C")



    ###################################################################################

    #Teacher disables the chat access provided.

    def test_test14(self):
        jit3 = Jitsi(driver2)
        # Switching to teacher screen
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        time.sleep(5)
        jit.click_chat_button()
        jit.get_messages_count(3)
        jit.click_settings_button()
        jit.enable_chat_forAll()
        jit.click_exit_button_Settings()

        #jit2.switchIframe()
      #  jit2.click_ok_button()
        #jit2.click_participant_button()
       # jit2.validateChatState()


    def test_test15(self):
        driver.close()
        driver.quit()
        driver2.close()
        driver2.quit()


    ###################################################################################


