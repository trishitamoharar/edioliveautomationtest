import time

import allure
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pytest


from selenium.webdriver.support.events import EventFiringWebDriver, AbstractEventListener



#Working code. Do not delete
from EdioLive.pageObjects.CoursePage import CoursePage
from EdioLive.pageObjects.DashboardPage import DashboardPage
from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class Test(BaseClass):


    @pytest.mark.smoke

    def test_test8(self):
        global teacherHandle, studentHandle, jit, jit2, driver, cp2, dash2, TeacherHandleDefault, defaultstudentHandle

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)
     #   driver = EventFiringWebDriver(driver_chrome, MyListener())
        driver.get(TestData.URL)
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)
        jit = Jitsi(driver)

        #dash = login.enterLoginCredentials()
        dash = login.enter_credentials(TestData.TEACHER_USERNAME1,TestData.TEACHER_PASSWORD)
        TeacherHandleDefault = dash.enter_classroom1()
        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()
        jit.validate_default_state_of_teacher()
        jit.validate_visibility_Of_sidePanel_elements_teacher()
        jit.click_camera_button()
        jit.click_mic_button()
        jit.click_camera_button()
        jit.click_mic_button()

    ###################################################################################

    #Student enters the session and requests for camera access
    # and Teacher allows the camera access. Student should be able to access camera.

    @pytest.mark.regression
    def test_test9(self):

        global cp3, driver2, cp4
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH, options=chrome_options)

        #  driver2 = EventFiringWebDriver(driver_chrome2, MyListener())
        driver2.get(TestData.URL)
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        jit2 = Jitsi(driver2)
        cp3 = CoursePage(driver2)
        #cp4 = CoursePage(driver)

        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1,TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        ###################defaultstudentHandle = dash2.getDefaultWindowHandle()
        jit2.click_joinMeeting_button()
        jit2.validate_default_state_of_student()
        jit2.request_camera_access()

        studentHandle = dash2.getDefaultWindowHandle()
        print("Student :" + studentHandle)

        # Switching to teacher screen
        driver.switch_to.window(teacherHandle)
        jit.click_allow_button()
        driver2.switch_to.window(studentHandle)
        jit2.switchIframe()
        jit2.click_ok_button()
        jit2.click_camera_button_Student()

        #jit2.click_exit_button()
        driver2.quit()


    #################################################################################

    # Student enters the session and requests for camera access
    # and Teacher allows the camera access. Student should not be able to access camera.

    @pytest.mark.smoke
    def test_test10(self):
        global cp3, driver2, cp4
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH, options=chrome_options)

        #  driver2 = EventFiringWebDriver(driver_chrome2, MyListener())
        driver2.get(TestData.URL)
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        jit2 = Jitsi(driver2)
        cp3 = CoursePage(driver2)
        # cp4 = CoursePage(driver)

        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        ###################defaultstudentHandle = dash2.getDefaultWindowHandle()
        jit2.click_joinMeeting_button()
        jit2.validate_default_state_of_student()
        jit2.request_camera_access()
        studentHandle = dash2.getDefaultWindowHandle()
        print("Student :" + studentHandle)
        # Switching to teacher screen
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.reject_camera_access()
        #jit.clickMic()

        driver2.switch_to.window(studentHandle)
        jit2.switchIframe()
        jit2.validate_default_state_of_student()
        #jit2.click_exit_button()

    def test_test11(self):
        driver2.close()
        driver2.quit()

        driver.switch_to.window(teacherHandle)
        driver.close()
        driver.quit()

        #################################################################

