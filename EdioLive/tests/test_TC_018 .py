
from selenium import webdriver
import pytest


from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class Test(BaseClass):



    def test_test76(self):
        global teacherHandle, jit,jit2,jit3, driver,driver2,driver3, cp2, dash2, TeacherHandleDefault, defaultstudentHandle,studentHandle2,studentHandle1

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                  options=chrome_options)
        #   driver = EventFiringWebDriver(driver_chrome, MyListener())

        driver.get("https://demo-edio.ccaeducate.me/")
        driver.maximize_window()
        driver.implicitly_wait(120)
        login = LoginPage(driver)


        dash = login.enter_credentials(TestData.TEACHER_USERNAME1, TestData.TEACHER_PASSWORD)
        jit = dash.enter_classroom1()
        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()
      #  jit.validate_default_state_of_teacher()
       # jit.validate_visibility_Of_sidePanel_elements_teacher()
        ###################################################################################


        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                   options=chrome_options)

        driver2.get("https://demo-edio.ccaeducate.me/")
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        jit2 = Jitsi(driver2)

        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        studentHandle1=dash2.getDefaultWindowHandle()
        jit2.validate_default_state_of_student()




    ###################################################################################

    #validate options under more action button
    def test_test77(self):
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.click_more_actions_button()
        jit.validate_options_under_more_action_button()
       # jit.click_more_actions_button()

        jit.click_and_validate_video_quality_box()
        jit.click_more_actions_button()
        jit.click_and_validate_fullscreen_button()
        jit.click_more_actions_button()
        jit.click_and_validate_settings_button()
        jit.click_more_actions_button()
        jit.click_and_validate_speaker_stats()

    #validate more action options for Student
    def test_test78(self):
        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        jit2.click_more_actions_button_Student()
        jit2.click_and_validate_video_quality_box()
        jit2.click_more_actions_button_Student()
        jit2.click_and_validate_fullscreen_button()
        # jit.click_more_actions_button()
        jit2.click_and_validate_settings_button()

    def test_test79(self):

        driver.close()
        driver.quit()
        driver2.switch_to.window(studentHandle1)
        driver2.close()
        driver2.quit()















