import time

from numpy.core.defchararray import strip
from selenium import webdriver
import pytest
from selenium.webdriver import ActionChains

from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class Test(BaseClass):


    def test_test90(self):
        global teacherHandle, jit, jit2, jit3, driver, driver2, driver3, cp2, dash2, TeacherHandleDefault, defaultstudentHandle, studentHandle2, studentHandle1

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                  options=chrome_options)
        #   driver = EventFiringWebDriver(driver_chrome, MyListener())

        log = self.getLogger()
        driver.get("https://demo-edio.ccaeducate.me/")
        log.info("Logged into EDIO")

        driver.maximize_window()
        driver.implicitly_wait(120)
        login = LoginPage(driver)

        dash = login.enter_credentials(TestData.TEACHER_USERNAME1, TestData.TEACHER_PASSWORD)
        log.info("Entered the Valid Credentials")

        jit = dash.enter_classroom1()

        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()
    ###################################################################################

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)

        driver2.get("https://demo-edio.ccaeducate.me/")
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        jit2 = Jitsi(driver2)

        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        studentHandle1 = dash2.getDefaultWindowHandle()

    def test_test91(self):
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.click_participant_button()
        time.sleep(4)
        driver.find_element_by_xpath("//input[@placeholder='Find a participant']").send_keys("ABROMITIS")
        time.sleep(7)
        element = driver.find_element_by_xpath("(//div[@class='participant__profile'])[1]")
        hoverMethod = ActionChains(driver).move_to_element(element)
        hoverMethod.perform()
        jit.click_ThreeDots_button()
        time.sleep(2)
        element2 = driver.find_element_by_xpath("//button[text()='Chat']")
        element2.click()
        #action.move_to_element(element).click(driver.find_element_by_xpath("//span[@class='material-icons']")).click(driver.find_elements_by_xpath("//div[@class='dropdown-menu   show']/ul/li/button[text='Chat']"))
        message = "Sending a message"
        jit.send_chatMessages(message)

        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        jit2.click_chat_button()
        driver2.find_element_by_xpath("//div[@class=' css-yk16xz-control']").click()
        time.sleep(5)
        t = driver2.find_element_by_xpath("//div[@class=' css-1w9j89e-menu']/div")
        listval = t.find_elements_by_xpath("./child::*")
        print(listval)
        count = len(listval)
        print(count)
        print("*************")
        val = listval[0].text
        for i in listval:
            print(i.text)
            if "Voitek, Cayce" in i.text:
                assert "1" in i.text
                i.click()
        actualmessege = driver2.find_element_by_xpath("//div[@class='messagecontent']/div").text
        assert message == actualmessege



    def test_test92(self):
        driver.close()
        driver.quit()
        driver2.switch_to.window(studentHandle1)
        driver2.close()
        driver2.quit()

