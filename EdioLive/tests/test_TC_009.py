import time

import allure
from selenium import webdriver
import pytest
from selenium.webdriver.chrome import options



#Teacher clicks on "Accept All " button to accept all requests.
#Teacher clicks on "Clear All" button to cancel all requests.
#Count of requests
from EdioLive.pageObjects.CoursePage import CoursePage
from EdioLive.pageObjects.DashboardPage import DashboardPage
from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class Test(BaseClass):

    #test_Accept_All
    @pytest.mark.smoke



    def test_test36(self):

        log=self.getLogger()
        global teacherHandle, studentHandle, jit, jit2, driver,driver2, cp2, dash2, TeacherHandleDefault, defaultstudentHandle

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)
     #   driver = EventFiringWebDriver(driver_chrome, MyListener())
        driver.get(TestData.URL)
        log.info("Entered EDIO")

        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)
        jit = Jitsi(driver)

        #dash = login.enterLoginCredentials()
        dash = login.enter_credentials(TestData.TEACHER_USERNAME1,TestData.TEACHER_PASSWORD)
        log.info("Entered valid credentials")

        TeacherHandleDefault = dash.enter_classroom1()
        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()
        jit.validate_default_state_of_teacher()
        log.info("Validated the default state of Teacher")

        jit.validate_visibility_Of_sidePanel_elements_teacher()
        #jit.allow_Button

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                   options=chrome_options)

        driver2.get("https://demo-edio.ccaeducate.me/")
        driver2.maximize_window()
        driver2.implicitly_wait(120)
        jit2 = Jitsi(driver2)

        login2 = LoginPage(driver2)
        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        jit2.validate_default_state_of_student()






    ###################################################################################

    #sTUDENTS enter the session and teacher accepts all the request by clicking Accept All button.

    @pytest.mark.regression
    def test_test37(self):

        global cp3, cp4

        jit2.request_mic_access()
        jit2.request_camera_access()

        studentHandle = dash2.getDefaultWindowHandle()
        print("Student :" + studentHandle)

        # Switching to teacher screen
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.calculate_requestCount("2")
        jit.click_acceptAll_Button()
        driver2.switch_to.window(studentHandle)
        jit2.switchIframe()
        jit2.click_ok_button()
        time.sleep(2)
        jit2.click_ok_button1()
        jit2.click_mic_button_Student()
        jit2.click_camera_button_Student()
        #jit2.click_exit_button()
        time.sleep(5)
        driver2.quit()


    #################################################################################

    #Student enters the session and requests for mic access and Teacher denies the request by clicking clear all.

    @pytest.mark.smoke
    def test_test38(self):
        global jit2,dash2

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                   options=chrome_options)

        driver2.get("https://demo-edio.ccaeducate.me/")
        driver2.maximize_window()
        driver2.implicitly_wait(120)
        jit2 = Jitsi(driver2)

        login2 = LoginPage(driver2)
        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        jit2.validate_default_state_of_student()
        jit2.request_mic_access()
        jit2.request_camera_access()
        studentHandle = dash2.getDefaultWindowHandle()
        print("Student :" + studentHandle)
        # Switching to teacher screen
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.calculate_requestCount("2")
        jit.click_clearAll_Button()
        driver2.switch_to.window(studentHandle)
        jit2.switchIframe()
        jit2.validate_default_state_of_student()
        #jit2.click_exit_button()
        #################################################################


    def test_test39(self):
        driver2.close()
        driver2.quit()
        driver.switch_to.window(teacherHandle)
        driver.close()
        driver.quit()

        #################################################################


