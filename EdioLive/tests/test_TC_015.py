import time

import allure
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pytest


from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class Test(BaseClass):
    #test_DeleteSpecificMessages


    def test_test61(self):
        global teacherHandle, jit,jit2,jit3, driver,driver2,driver3, cp2, dash2, TeacherHandleDefault, defaultstudentHandle,studentHandle2,studentHandle1

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                  options=chrome_options)
        #   driver = EventFiringWebDriver(driver_chrome, MyListener())

        driver.get("https://demo-edio.ccaeducate.me/")
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)


        dash = login.enter_credentials(TestData.TEACHER_USERNAME1, TestData.TEACHER_PASSWORD)
        jit = dash.enter_classroom1()
        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()
        jit.validate_default_state_of_teacher()
        jit.validate_visibility_Of_sidePanel_elements_teacher()

        ###################################################################################


        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                   options=chrome_options)

        driver2.get("https://demo-edio.ccaeducate.me/")
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME3, TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        studentHandle1=dash2.getDefaultWindowHandle()
        jit2.validate_default_state_of_student()
        #jit2.click_ok_button()

    ###################################################################################

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver3 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                   options=chrome_options)
        #driver2 = EventFiringWebDriver(driver_chrome2, MyListener())
        driver3.get("https://demo-edio.ccaeducate.me/")
        driver3.maximize_window()
        driver3.implicitly_wait(120)
        login3 = LoginPage(driver3)
        dash3 = login3.enter_credentials(TestData.STUDENT_USERNAME4, TestData.STUDENT_PASSWORD1)
        jit3=dash3.enter_classroom2()
        jit3.click_joinMeeting_button()
        studentHandle2 = dash3.getDefaultWindowHandle()
        #jit3.validate_default_state_of_student()

        #jit3.click_ok_button()

    ###################################################################################

    # validate the delete specific message feature. On deleting a specific msg validate if
    # the msgs have grayed out and if the student is able to view the messages.
    def test_test62(self):
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.click_settings_button()
        jit.enable_chat_forAll()
        jit.click_exit_button_Settings()

        driver2.switch_to.window(studentHandle1)
        #jit2.switchIframe()
        jit2.click_ok_button()
        jit2.click_chat_button()
        jit2.send_chat_message_Student("A")
        jit2.send_chat_message_Student("B")
        jit2.send_chat_message_Student("C")
        jit2.send_chat_message_Student("D")
        jit2.send_chat_message_Student("E")
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.get_messages_count("5")
        jit.click_chat_button()
        jit.delete_specific_message()
        jit.validate_filtered_chat_messages("1")
        jit.collect_chat_messages("A")
        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        jit2.click_chat_button()
        #jit2.collect_chat_messages1("A")

    #Validate if the number of unread msgs is decreasing on deleting specific messages.
    def test_test63(self):
        driver3.switch_to.window(studentHandle2)
        jit3.click_ok_button()
      #  jit3.switchIframe()
        jit3.validate_chat_notification_count("4")

    #Validate the msgs count after deletion and when side panel is closed.
    def test_test64(self):
     #   driver3.switch_to.window(studentHandle2)
        jit3.click_close_sidePanel_Button()
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.click_three_dots_chat_Button()
        jit.click_clearChat_history_Button()
        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        jit2.click_chat_button()
        jit2.send_chat_message_Student("1")
        jit2.send_chat_message_Student("2")
        jit2.send_chat_message_Student("3")
        jit2.send_chat_message_Student("4")
        jit.delete_specific_message()
        jit.validate_filtered_chat_messages("1")
        jit.collect_chat_messages("1")
        driver3.switch_to.window(studentHandle2)
        jit3.switchIframe()
        jit3.validate_unread_notification_count("3")

    def test_test65(self):
        driver3.close()
        driver3.quit()
        driver2.close()
        driver2.quit()
        driver.close()
        driver.quit()





















