import time

import allure
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pytest

from EdioLive.pageObjects.CoursePage import CoursePage
from EdioLive.pageObjects.DashboardPage import DashboardPage
from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from selenium.webdriver.support.events import EventFiringWebDriver, AbstractEventListener

from EdioLive.utilities.MyListener import MyListener
from EdioLive.utilities.config import TestData


class Test(BaseClass):

    @allure.description("Setting up Precondition : Teacher enters the session.")
    @pytest.mark.smoke

    def test_test1(self):
        global teacherHandle, studentHandle, jit, jit2, driver, cp2, dash2, TeacherHandleDefault, defaultstudentHandle

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)
     #   driver = EventFiringWebDriver(driver_chrome, MyListener())
        driver.get(TestData.URL)
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)
        jit = Jitsi(driver)

        #dash = login.enterLoginCredentials()
        dash = login.enter_credentials(TestData.TEACHER_USERNAME,TestData.TEACHER_PASSWORD)
        TeacherHandleDefault = dash.enter_classroom()
        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()
        jit.validate_default_state_of_teacher()
        jit.validate_visibility_Of_sidePanel_elements_teacher()
        jit.click_camera_button()
        jit.click_mic_button()
        jit.click_camera_button()
        jit.click_mic_button()

    ###################################################################################

    @allure.description("Student enters the session and requests for screen share access and Teacher allows the access.")

    @pytest.mark.regression
    @pytest.mark.depends(on=['test_test1'])
    def test_test2(self):

        global cp3, driver2, cp4
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH, options=chrome_options)

        #  driver2 = EventFiringWebDriver(driver_chrome2, MyListener())
        driver2.get(TestData.URL)
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        jit2 = Jitsi(driver2)
        cp3 = CoursePage(driver2)
        cp4 = CoursePage(driver)

        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1,TestData.STUDENT_PASSWORD1)
        lp2 = dash2.click_learning_button()
        lp2.enter_value_in_searchBar()
        lp2.select_searchResult()
        lp2.click_progress_button()
        cp2 = lp2.click_unit_chapter()
        cp2.click_videoLink()
        defaultstudentHandle = cp2.clickLaunchButton2()
        jit2.click_joinMeeting_button()
        jit2.validate_default_state_of_student()
        jit2.request_screenShare_access()


        studentHandle = dash2.getDefaultWindowHandle()
        print("Student :" + studentHandle)

        # Switching to teacher screen
        driver.switch_to.window(teacherHandle)
        jit.click_allow_button()
        driver2.switch_to.window(studentHandle)
        jit2.switchIframe()
        jit2.click_ok_button()
        jit2.click_screenShare_button_Student()

        #jit2.click_exit_button()

        time.sleep(5)
        driver2.close()
        driver2.switch_to.window(defaultstudentHandle)

    #################################################################################

    @allure.description("Student enters the session and requests for mic access and Teacher denies the request.")

    @pytest.mark.smoke
    @pytest.mark.depends(on=['test_test1'])
    def test_test3(self):
        global dash3, cp5

        dash3 = DashboardPage(driver2)
        cp5 = CoursePage(driver)

        jit3 = cp3.click_launch_button()
        jit3.click_joinMeeting_button()

        jit3.validate_default_state_of_student()
        jit3.request_mic_access()

        studentHandle = dash3.getDefaultWindowHandle()
        print("Student :" + studentHandle)
        # Switching to teacher screen
        driver.switch_to.window(teacherHandle)
        jit.reject_mic_access()
        #jit.clickMic()

        driver2.switch_to.window(studentHandle)
        jit3.switchIframe()
        jit3.validate_default_state_of_student()

        #################################################################

        # jit3.click_exit_button()
        # driver2.close()
        # driver2.quit()
        #
        # driver.switch_to.window(teacherHandle)
        # driver.close()
        # driver.quit()

        #################################################################

