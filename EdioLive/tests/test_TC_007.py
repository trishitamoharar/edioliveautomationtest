import time

import allure
from selenium import webdriver
import pytest
from selenium.webdriver import ActionChains

from EdioLive.pageObjects.CoursePage import CoursePage
from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class Test(BaseClass):

    #test_Allow_Disable_Camera
    @pytest.mark.smoke



    def test_test28(self):
        global teacherHandle, studentHandle, jit, driver, cp2, dash2, TeacherHandleDefault,jit2, driver2, defaultstudentHandle

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)
     #   driver = EventFiringWebDriver(driver_chrome, MyListener())
        driver.get(TestData.URL)
        driver.maximize_window()
        driver.implicitly_wait(60)

        login = LoginPage(driver)
        jit = Jitsi(driver)

        dash = login.enter_credentials(TestData.TEACHER_USERNAME1,TestData.TEACHER_PASSWORD)
        TeacherHandleDefault = dash.enter_classroom1()
        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()
        jit.validate_default_state_of_teacher()
        jit.validate_visibility_Of_sidePanel_elements_teacher()

    ###################################################################################

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                   options=chrome_options)

        driver2.get("https://demo-edio.ccaeducate.me/")
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        studentHandle1 = dash2.getDefaultWindowHandle()
        jit2.validate_default_state_of_student()

    # Validate if the options across specific participants are working as per commands. Here validate
    # if the Allow camera gives access to only a specific participant

    @pytest.mark.regression
    def test_test29(self):

        global  studentHandle

        studentHandle = dash2.getDefaultWindowHandle()
        print("Student :" + studentHandle)

        driver.switch_to.window(teacherHandle)
        jit.switchIframe()

        element = driver.find_element_by_xpath("(//div[@class='participant__profile'])[1]")
        hoverMethod = ActionChains(driver).move_to_element(element)
        hoverMethod.perform()
        jit.click_ThreeDots_button()
        jit.click_allow_video_Button()
        driver2.switch_to.window(studentHandle)
        jit2.switchIframe()
        jit2.click_ok_button()
        jit2.click_camera_button_Student()

    #################################################################################


    # Validate if Disable mic only disables the specific participant.
    def test_test30(self):

        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        element = driver.find_element_by_xpath("(//div[@class='participant__profile'])[1]")
        hoverMethod = ActionChains(driver).move_to_element(element)
        hoverMethod.perform()
        jit.click_ThreeDots_button()
        jit.click_disable_video_Button()
        driver2.switch_to.window(studentHandle)
        jit2.switchIframe()
        jit2.validate_default_state_of_student()
        #jit2.click_exit_button()

    def test_test31(self):
        driver2.close()
        driver2.quit()
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        #jit.click_exit_button()
        driver.close()
        driver.quit()


