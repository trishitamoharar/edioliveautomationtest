import time

import allure
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pytest


from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class Test(BaseClass):
    #test_DeleteSpecificMessages


    def test_test96(self):
        global teacherHandle, jit,jit2,jit3, driver,driver2,driver3, cp2, dash2, TeacherHandleDefault, defaultstudentHandle,studentHandle2,studentHandle1

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                  options=chrome_options)
        #   driver = EventFiringWebDriver(driver_chrome, MyListener())

        driver.get("https://demo-edio.ccaeducate.me/")
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)


        dash = login.enter_credentials(TestData.TEACHER_USERNAME1, TestData.TEACHER_PASSWORD)
        jit = dash.enter_classroom1()
        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()

        ###################################################################################


        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                   options=chrome_options)

        driver2.get("https://demo-edio.ccaeducate.me/")
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2=dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        studentHandle1=dash2.getDefaultWindowHandle()
        #jit2.click_ok_button()

    ###################################################################################

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver3 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                   options=chrome_options)
        #driver2 = EventFiringWebDriver(driver_chrome2, MyListener())
        driver3.get("https://demo-edio.ccaeducate.me/")
        driver3.maximize_window()
        driver3.implicitly_wait(120)
        login3 = LoginPage(driver3)
        dash3 = login3.enter_credentials(TestData.STUDENT_USERNAME2, TestData.STUDENT_PASSWORD1)
        jit3=dash3.enterClassroom_Student()
        jit3.click_joinMeeting_button()
        studentHandle2 = dash3.getDefaultWindowHandle()


    def test_test97(self):
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        driver.find_element_by_xpath("//div[@class='button-group-right']/div[1]/div/button").click()
        time.sleep(3)
        # driver.find_element_by_xpath("//label[@labelfor='chatSwitch']/span").click()
        driver.find_element_by_xpath("//label[@labelfor='stdTechChatSwitch']/span").click()
        time.sleep(2)
        ele1 = driver.find_element_by_xpath(
            "//div[@class='Content__DefaultWrapperComponent-sc-1npw367-0 ffdVFW']/div[1]/h4/div")
        driver.execute_script("arguments[0].click();", ele1)

        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        time.sleep(5)
        ele = driver2.find_element_by_xpath("//div[@class='popup_overlay show-notification']/div/div/button")
        driver2.execute_script("arguments[0].click();", ele)
        jit2.click_chat_button()
        driver2.find_element_by_xpath("//div[@class=' css-yk16xz-control']").click()
        time.sleep(5)
        t = driver2.find_element_by_xpath("//div[@class=' css-1w9j89e-menu']/div")
        listval = t.find_elements_by_xpath("./child::*")
        print(listval)
        count = len(listval)
        print(count)
        print("*************")
        for i in listval:
            #print(i.text)
            if "Voitek, Cayce" in i.text:
                i.click()
        studentcount1 = 2
        message1 = "Student message1"
        message2 = "student message2"
        jit2.send_chatMessages(message1)
        jit2.send_chatMessages(message2)

        driver3.switch_to.window(studentHandle2)
        jit3.switchIframe()
        time.sleep(5)
        ele = driver3.find_element_by_xpath("//div[@class='popup_overlay show-notification']/div/div/button")
        driver3.execute_script("arguments[0].click();", ele)
        jit3.click_chat_button()
        driver3.find_element_by_xpath("//div[@class=' css-yk16xz-control']").click()
        time.sleep(5)
        t = driver3.find_element_by_xpath("//div[@class=' css-1w9j89e-menu']/div")
        listval = t.find_elements_by_xpath("./child::*")
        print(listval)
        count = len(listval)
        print(count)
        print("*************")
        val = listval[0].text
        for i in listval:
            # print(i.text)
            if "Voitek, Cayce" in i.text:
                i.click()
        studentcount2 = 2
        message3 = "Student message3"
        message4 = "student message4"
        jit3.send_chatMessages(message3)
        jit3.send_chatMessages(message4)



        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.click_chat_button()
        chatcountstu = int(driver.find_element_by_xpath("//div[@class='participent-tab']/ul/li[1]/a/span[2]").text)
        assert chatcountstu == studentcount1 + studentcount2
        driver.find_element_by_xpath("//div[@class=' css-yk16xz-control']").click()
        time.sleep(4)
        t = driver.find_element_by_xpath("//div[@class=' css-1w9j89e-menu']/div")
        listval = t.find_elements_by_xpath("./child::*")
        print("*************")
        for i in listval:
            print(i.text)
            assert 2 in i.text




    def test_test98(self):
        driver.close()
        driver.quit()
        driver2.switch_to.window(studentHandle1)
        driver2.close()
        driver2.quit()