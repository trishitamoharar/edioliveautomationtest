import time

import allure
from selenium import webdriver
import pytest

from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class Test(BaseClass):

    #MIC FOR ALL


    def test_test16(self):
        global teacherHandle, jit, driver, cp2, dash2, TeacherHandleDefault, defaultstudentHandle,driver2,jit2

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                  options=chrome_options)
     #   driver = EventFiringWebDriver(driver_chrome, MyListener())


        driver.get("https://demo-edio.ccaeducate.me/")
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)
        jit = Jitsi(driver)

        dash = login.enter_credentials(TestData.TEACHER_USERNAME1, TestData.TEACHER_PASSWORD)
        TeacherHandleDefault = dash.enter_classroom1()
        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()
        jit.validate_default_state_of_teacher()
        jit.validate_visibility_Of_sidePanel_elements_teacher()

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                   options=chrome_options)

        driver2.get("https://demo-edio.ccaeducate.me/")
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        studentHandle1 = dash2.getDefaultWindowHandle()
        jit2.validate_default_state_of_student()


    ###################################################################################

   #Student enters the session and Teacher allows mic access to all students.

    def test_test17(self):

        global studentHandle

        studentHandle = dash2.getDefaultWindowHandle()
        print("Student :" + studentHandle)
        # Switching to teacher screen
        driver.switch_to.window(teacherHandle)
        jit.switch_Iframe()
        jit.click_settings_button()
        jit.click_enable_Mic_slider()
        jit.click_exit_button_Settings()
        driver2.switch_to.window(studentHandle)
        jit2.switchIframe()
        jit2.click_ok_button()
        time.sleep(5)
        jit2.click_mic_button_Student()

    ###################################################################################

    #Teacher disables the mic access provided.

    def test_test18(self):
        # Switching to teacher screen
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.click_settings_button()
        jit.click_enable_Mic_slider()
        jit.click_exit_button_Settings()


        driver2.switch_to.window(studentHandle)
        jit2.switchIframe()
        jit2.validate_default_state_of_student()

      ###################################################################################

    def test_tetest19(self):
        driver2.close()
        driver2.quit()
        driver.switch_to.window(teacherHandle)
        driver.close()
        driver.quit()



