import time

from numpy.core.defchararray import strip
from selenium import webdriver
import pytest

from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class Test(BaseClass):


    def test_test87(self):
        global teacherHandle, jit, jit2, jit3, driver, driver2, driver3, cp2, dash2, TeacherHandleDefault, defaultstudentHandle, studentHandle2, studentHandle1

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                  options=chrome_options)
        #   driver = EventFiringWebDriver(driver_chrome, MyListener())

        log = self.getLogger()
        driver.get("https://demo-edio.ccaeducate.me/")
        log.info("Logged into EDIO")

        driver.maximize_window()
        driver.implicitly_wait(120)
        login = LoginPage(driver)

        dash = login.enter_credentials(TestData.TEACHER_USERNAME1, TestData.TEACHER_PASSWORD)
        log.info("Entered the Valid Credentials")

        jit = dash.enter_classroom1()

        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()
    ###################################################################################

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)

        driver2.get("https://demo-edio.ccaeducate.me/")
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        jit2 = Jitsi(driver2)

        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        studentHandle1 = dash2.getDefaultWindowHandle()

    def test_test88(self):
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.click_chat_button()
        time.sleep(5)

        driver.find_element_by_xpath("//div[@class=' css-yk16xz-control']").click()
        time.sleep(2)
        t = driver.find_element_by_xpath("//div[@class=' css-1w9j89e-menu']/div")
        listval = t.find_elements_by_xpath("./child::*")
        print(listval)
        count = len(listval)
        print(count)
        print("*************")
        val = listval[0].text
        #expectedval = val.split('\n')[1]
        expectedval = "ABROMITIS, MACY"
        assert count > 1
        for i in listval:
            print(i.text)
            if "ABROMITIS, MACY" in i.text:
                i.click()
        #comparing studentname
        actualval = driver.find_element_by_xpath("//div[@class=' css-1hwfws3']/div").text
        assert actualval==expectedval
        message = "Hello Student"
        jit.send_chatMessages(message)

        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        jit2.click_chat_button()
        driver2.find_element_by_xpath("//div[@class=' css-yk16xz-control']").click()
        time.sleep(5)
        t = driver2.find_element_by_xpath("//div[@class=' css-1w9j89e-menu']/div")
        listval = t.find_elements_by_xpath("./child::*")
        print(listval)
        count = len(listval)
        print(count)
        print("*************")
        val = listval[0].text
        #listval[0].click()

        for i in listval:
            print(i.text)
            if "Voitek, Cayce" in i.text:
                i.click()

        actualmessege = driver2.find_element_by_xpath("//div[@class='messagecontent']/div").text
        assert message == actualmessege

        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        driver.find_element_by_xpath("//div[@class='css-1g6gooi']/div/input").send_keys(TestData.SEARCHNAME_PARTIALVALUE_STUDENT1)
        time.sleep(4)
        t = driver.find_element_by_xpath("//div[@class=' css-1w9j89e-menu']/div")
        listval = t.find_elements_by_xpath("./child::*")
        for i in listval:
            print(i.text)
            if "ABROMITIS, MACY" in i.text:
                i.click()
        actualname = driver.find_element_by_xpath("//div[@class=' css-1hwfws3']/div").text
        assert expectedval == actualname

    def test_test89(self):
        driver.close()
        driver.quit()
        driver2.switch_to.window(studentHandle1)
        driver2.close()
        driver2.quit()














