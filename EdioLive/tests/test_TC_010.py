import time

import allure
from selenium import webdriver
import pytest
from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class Test(BaseClass):

    #test_search

    def test_test40(self):
        global teacherHandle, jit,jit2,jit3, driver,driver2,driver3, cp2, dash2, TeacherHandleDefault, defaultstudentHandle,studentHandle2,studentHandle1

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                  options=chrome_options)

        driver.get("https://demo-edio.ccaeducate.me/")
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)
       # jit = Jitsi(driver)

        dash = login.enter_credentials(TestData.TEACHER_USERNAME1, TestData.TEACHER_PASSWORD)
        jit = dash.enter_classroom1()
        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()
      #  jit.validate_default_state_of_teacher()
       # jit.validate_visibility_Of_sidePanel_elements_teacher()
        ###################################################################################


        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                   options=chrome_options)

        driver2.get("https://demo-edio.ccaeducate.me/")
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        jit2 = Jitsi(driver2)

        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2=dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        studentHandle1=dash2.getDefaultWindowHandle()
       # jit2.validate_default_state_of_student()

    ###################################################################################

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver3 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                   options=chrome_options)
        #  driver2 = EventFiringWebDriver(driver_chrome2, MyListener())
        driver3.get("https://demo-edio.ccaeducate.me/")
        driver3.maximize_window()
        driver3.implicitly_wait(120)

        login3 = LoginPage(driver3)
        jit3 = Jitsi(driver3)

        dash3 = login3.enter_credentials(TestData.STUDENT_USERNAME2, TestData.STUDENT_PASSWORD1)
        jit3=dash3.enterClassroom_Student()
        jit3.click_joinMeeting_button()
        studentHandle2 = dash3.getDefaultWindowHandle()
        #jit3.validate_default_state_of_student()


    ###################################################################################

    #validate the search feature for a teacher
    def test_test41(self):
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.enter_search_input(TestData.SEARCHNAME_FIRSTNAME_STUDENT)
        jit.validate_searchbar_result(TestData.SEARCHNAME_FIRSTNAME_STUDENT)

        jit.enter_search_input(TestData.SEARCHNAME_LASTNAME_STUDENT)
        jit.validate_searchbar_result(TestData.SEARCHNAME_LASTNAME_STUDENT)

        jit.enter_search_input(TestData.SEARCHNAME_PARTIALVALUE_STUDENT1)
        jit.validate_searchbar_result(TestData.SEARCHNAME_PARTIALVALUE_STUDENT1)

        jit.enter_search_input("#######")
        jit.validate_zero_searchBar_results()
        #jit.click_exit_button()

    # validate the search feature for a Student
    def test_test42(self):
        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        jit2.enter_search_input(TestData.SEARCHNAME_FIRSTNAME_STUDENT)
        jit2.validate_searchbar_result(TestData.SEARCHNAME_FIRSTNAME_STUDENT)

        jit2.enter_search_input(TestData.SEARCHNAME_LASTNAME_STUDENT)
        jit2.validate_searchbar_result(TestData.SEARCHNAME_LASTNAME_STUDENT)

        jit2.enter_search_input(TestData.SEARCHNAME_PARTIALVALUE_STUDENT1)
        jit2.validate_searchbar_result(TestData.SEARCHNAME_PARTIALVALUE_STUDENT1)

        jit2.enter_search_input("#######")
        jit2.validate_zero_searchBar_results()

        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        #jit.click_exit_button()

    def test_test43(self):
        driver.close()
        driver.quit()
        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        #jit2.click_exit_button()
        driver2.close()
        driver2.quit()
        driver3.switch_to.window(studentHandle2)
        jit3.switchIframe()
        #jit3.click_exit_button()
        driver3.close()
        driver3.quit()










