import datetime
import os
import time

import allure
from selenium import webdriver
import pytest

from EdioLive.pageObjects.CoursePage import CoursePage
from EdioLive.pageObjects.DashboardPage import DashboardPage
from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class Test(BaseClass):

    def test_teacherlogin(self):
        global teacherHandle, studentHandle, jit, jit2, driver, cp2, dash2, TeacherHandleDefault, defaultstudentHandle

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        print(TestData.CHROME_PATH)
        # driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH, options=chrome_options)
        #   driver = EventFiringWebDriver(driver_chrome, MyListener())
        driver.get(TestData.URL)
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)
        jit = Jitsi(driver)

        # dash = login.enterLoginCredentials()
        dash = login.enter_credentials(TestData.TEACHER_USERNAME1, TestData.TEACHER_PASSWORD)
        dash.validate_role(TestData.Role_Teacher)
        dash.validate_name(TestData.Name_Teacher)
        window_before = driver.window_handles[0]
        dash.validate_recordingtable()
        dash.click_launchbutton()
        window_after = driver.window_handles[1]
        driver.switch_to.window(window_after)
        jit.click_joinMeeting_button()

        teacherHandle = dash.getDefaultWindowHandle()


    def test_sysadmin(self):
        global teacherHandle, studentHandle, jit, jit2, driver, cp2, dash2, TeacherHandleDefault, defaultstudentHandle

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        print(TestData.CHROME_PATH)
        # driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH, options=chrome_options)
        #   driver = EventFiringWebDriver(driver_chrome, MyListener())
        driver.get(TestData.URL)
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)
        jit = Jitsi(driver)

        # dash = login.enterLoginCredentials()
        dash = login.enter_credentials(TestData.SYSADMIN_USERNAME, TestData.TEACHER_PASSWORD)
        dash.validate_role(TestData.Role_sysadmin)
        dash.validate_name(TestData.Name_sysadmin)
        window_before = driver.window_handles[0]
        dash.validate_recordingtable()
        dash.click_launchbutton()
        window_after = driver.window_handles[1]
        driver.switch_to.window(window_after)
        jit.click_joinMeeting_button()


    def test_schooladminlogin(self):
        global teacherHandle, studentHandle, jit, jit2, driver, cp2, dash2, TeacherHandleDefault, defaultstudentHandle

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        print(TestData.CHROME_PATH)
        # driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH, options=chrome_options)
        #   driver = EventFiringWebDriver(driver_chrome, MyListener())
        driver.get(TestData.URL)
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)
        jit = Jitsi(driver)

        # dash = login.enterLoginCredentials()
        dash = login.enter_credentials(TestData.SCHOOLADMIN_USERNAME, TestData.TEACHER_PASSWORD)
        dash.validate_role(TestData.Role_schooladmin)
        dash.validate_name(TestData.Name_schooladmin)
        window_before = driver.window_handles[0]
        dash.validate_recordingtable()
        dash.click_launchbutton()
        window_after = driver.window_handles[1]
        driver.switch_to.window(window_after)
        jit.click_joinMeeting_button()

        teacherHandle = dash.getDefaultWindowHandle()


    # def test_student(self):
    #     global teacherHandle, studentHandle, jit, jit2, driver, cp2, dash2, TeacherHandleDefault, defaultstudentHandle
    #
    #     chrome_options = webdriver.ChromeOptions()
    #     chrome_options.add_argument("--use-fake-ui-for-media-stream")
    #     print(TestData.CHROME_PATH)
    #     # driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)
    #     driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH, options=chrome_options)
    #     #   driver = EventFiringWebDriver(driver_chrome, MyListener())
    #     driver.get(TestData.URL)
    #     driver.maximize_window()
    #     driver.implicitly_wait(120)
    #
    #     login = LoginPage(driver)
    #     jit = Jitsi(driver)
    #
    #     # dash = login.enterLoginCredentials()
    #     dash = login.enter_credentials(TestData.STUDENTNEW_USERNAME, TestData.TEACHER_PASSWORD)
    #     dash.validate_role(TestData.Role_student)
    #     dash.validate_name(TestData.Name_student)
    #     window_before = driver.window_handles[0]
    #     dash.validate_recordingtable()
    #     dash.click_launchbutton()
    #     window_after = driver.window_handles[1]
    #     driver.switch_to.window(window_after)
    #     jit.click_joinMeeting_button()

    #   teacherHandle = dash.getDefaultWindowHandle()


    def test_familymentor(self):
        global teacherHandle, studentHandle, jit, jit2, driver, cp2, dash2, TeacherHandleDefault, defaultstudentHandle

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        print(TestData.CHROME_PATH)
        # driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH, options=chrome_options)
        #   driver = EventFiringWebDriver(driver_chrome, MyListener())
        driver.get(TestData.URL)
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)
        jit = Jitsi(driver)

        # dash = login.enterLoginCredentials()
        dash = login.enter_credentials(TestData.FAMILYMENTOR_USERNAME, TestData.TEACHER_PASSWORD)
        dash.validate_role(TestData.Role_familymentor)
        dash.validate_name(TestData.Name_familymentor)
        window_before = driver.window_handles[0]
        dash.validate_recordingtable()
        dash.click_launchbutton()
        window_after = driver.window_handles[1]
        driver.switch_to.window(window_after)
        jit.click_joinMeeting_button()

        teacherHandle = dash.getDefaultWindowHandle()



    def test_learningcoach(self):
        global teacherHandle, studentHandle, jit, jit2, driver, cp2, dash2, TeacherHandleDefault, defaultstudentHandle

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        print(TestData.CHROME_PATH)
        # driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,options=chrome_options)
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH, options=chrome_options)
        #   driver = EventFiringWebDriver(driver_chrome, MyListener())
        driver.get(TestData.URL)
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)
        jit = Jitsi(driver)

        # dash = login.enterLoginCredentials()
        dash = login.enter_credentials(TestData.LEARNINGCOACH_USERNAME, TestData.TEACHER_PASSWORD)
        dash.validate_restrictedlogin()