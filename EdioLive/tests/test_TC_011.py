import time

import allure
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pytest

from EdioLive.pageObjects.CoursePage import CoursePage
from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class Test(BaseClass):

    #test_clearChatHistory

    def test_test44(self):
        global teacherHandle, jit, driver, cp2, dash2,driver2,jit2, defaultstudentHandle,defaultTeacherHandle

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                  options=chrome_options)
     #   driver = EventFiringWebDriver(driver_chrome, MyListener())


        driver.get("https://demo-edio.ccaeducate.me/")
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)


        dash = login.enter_credentials(TestData.TEACHER_USERNAME1, TestData.TEACHER_PASSWORD)
        defaultTeacherHandle = dash.getDefaultWindowHandle()
        jit = dash.enter_classroom1()
        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()
      #  jit.validate_default_state_of_teacher()
      #  jit.validate_visibility_Of_sidePanel_elements_teacher()

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH, options=chrome_options)

        #  driver2 = EventFiringWebDriver(driver_chrome2, MyListener())
        driver2.get(TestData.URL)
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        jit2 = Jitsi(driver2)
        cp3 = CoursePage(driver2)
        #  cp4 = CoursePage(driver)

        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        jit2.validate_default_state_of_student()

    ###################################################################################

   #Validate if the clear all chat history button, grays out messagses for Teacher and
   # displays nothing to students.

    def test_test45(self):
        global studentHandle,cp

        studentHandle = dash2.getDefaultWindowHandle()
        print("Student :" + studentHandle)

        # Switching to teacher screen
        driver.switch_to.window(teacherHandle)
        jit.switch_Iframe()
        jit.click_settings_button()
        jit.enable_chat_forAll()
        jit.click_exit_button_Settings()
        driver2.switch_to.window(studentHandle)
        jit2.switchIframe()
        jit2.click_ok_button()
        jit2.click_chat_button()
        jit2.send_chatMessages("A")
        jit2.send_chatMessages("B")
        jit2.send_chatMessages("C")
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.click_chat_button()
        jit.get_messages_count(3)
        jit.click_three_dots_chat_Button()
        jit.click_clearChat_history_Button()
        jit.validate_filtered_chat_messages("3")
        driver2.switch_to.window(studentHandle)
        jit2.validate_blank_chatArea()


    ###################################################################################
    #Validate if on teacher leaving the classroom, the previosuly deleted chats are they visible to students.

    def test_test46(self):

        cp3=CoursePage(driver)
        # Switching to teacher screen
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        #jit.click_exit_button()
        driver.close()
        driver.switch_to.window(defaultTeacherHandle)
        jit3 = cp3.click_launch_button()
        jit3.click_joinMeeting_button()
        jit3.validate_filtered_chat_messages("3")
        driver2.switch_to.window(studentHandle)
        jit2.switchIframe()
        jit2.click_ok_button()
        jit2.validate_blank_chatArea()

    def test_test47(self):

        driver2.close()
        driver2.quit()
        driver.close()
        driver.quit()

    ###################################################################################


