
import allure
from selenium import webdriver
import pytest
from selenium.webdriver import ActionChains

from EdioLive.pageObjects.Jitsi import Jitsi
from EdioLive.pageObjects.LoginPage import LoginPage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData

#test_sidePanelClosed
class Test(BaseClass):

    @allure.severity(severity_level="CRITICAL")

    def test_test56(self):
        global teacherHandle, jit,jit2,jit3, driver,driver2,driver3, cp2, dash2, TeacherHandleDefault, defaultstudentHandle,studentHandle2,studentHandle1

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        driver = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                  options=chrome_options)


        driver.get("https://demo-edio.ccaeducate.me/")
        driver.maximize_window()
        driver.implicitly_wait(120)

        login = LoginPage(driver)


        dash = login.enter_credentials(TestData.TEACHER_USERNAME1, TestData.TEACHER_PASSWORD)
        jit = dash.enter_classroom1()
        jit.click_joinMeeting_button()
        teacherHandle = dash.getDefaultWindowHandle()

      #  jit.validate_default_state_of_teacher()
       # jit.validate_visibility_Of_sidePanel_elements_teacher()
        jit.click_close_sidePanel_Button()
        ###################################################################################


        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        driver2 = webdriver.Chrome(executable_path=TestData.CHROME_PATH,
                                   options=chrome_options)

        driver2.get("https://demo-edio.ccaeducate.me/")
        driver2.maximize_window()
        driver2.implicitly_wait(120)

        login2 = LoginPage(driver2)
        jit2 = Jitsi(driver2)

        dash2 = login2.enter_credentials(TestData.STUDENT_USERNAME1, TestData.STUDENT_PASSWORD1)
        jit2 = dash2.enterClassroom_Student()
        jit2.click_joinMeeting_button()
        studentHandle1=dash2.getDefaultWindowHandle()
       # jit2.validate_default_state_of_student()

    ###################################################################################

    #validate if the notification count is proper and if we are able to open and close side panel.Also check
    # if after clicking on Allow option student is able to receive the requested access
    def test_test57(self):
        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        jit2.request_mic_access()
        jit2.request_camera_access()
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.validate_sidePanel_notification_Count("2")
        jit.click_open_sidePanel()
        jit.click_acceptAll_Button()
        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
       # jit2.switchIframe()
        jit2.click_ok_button()
        jit2.click_ok_button1()
        jit2.click_mic_button_Student()
        jit2.click_camera_button_Student()


    #validate if side panel is displaying proper notification count if its able to open
    #close side panel. Also click on clear All buttons and check if student has not received the access.
    def test_test58(self):
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        element = driver.find_element_by_xpath("(//div[@class='participant-list__item '])[1]")
        hoverMethod = ActionChains(driver).move_to_element(element)
        hoverMethod.perform()
        jit.click_ThreeDots_button()
        jit.click_disable_video_Button()
        hoverMethod.perform()
        jit.click_ThreeDots_button()
        jit.click_disable_mic_Button()
        jit.click_close_sidePanel_Button()
        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        jit2.request_mic_access()
        jit2.request_camera_access()
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.validate_sidePanel_notification_Count("2")
        jit.click_open_sidePanel()
        jit.click_clearAll_Button()
        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        jit2.validate_default_state_of_student()


    #validate if side panel is displaying proper notification count if its able to open
    #close side panel. Also click on allow buttons and check if student has received the access.
    def test_test59(self):
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.click_close_sidePanel_Button()
        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        jit2.request_mic_access()
        driver.switch_to.window(teacherHandle)
        jit.switchIframe()
        jit.validate_sidePanel_notification_Count("1")
        jit.click_open_sidePanel()




    def test_test60(self):

        # jit.click_exit_button()
        driver.close()
        driver.quit()
        driver2.switch_to.window(studentHandle1)
        jit2.switchIframe()
        #jit2.click_exit_button()
        driver2.close()
        driver2.quit()










