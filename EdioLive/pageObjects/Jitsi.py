import time

import allure
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from EdioLive.utilities.BaseClass import BaseClass



class Jitsi(BaseClass):

    def __init__(self, driver):
        self.driver=driver
    global countOfRequests

    #joinMeeting_button=(By.XPATH, "//button[@class='button-prejoin sc-chPdSV dKdtrl']")
    joinMeeting_button=(By.XPATH, "//span[text()='Join Meeting']")
    camera_Button=(By.XPATH, "//div[@class='button-group-center']/div[2]")
    camera1=(By.XPATH, "//div[@class='toolbox-icon toggled  ']/div")
    mic_Button = (By.XPATH, "//div[@class='button-group-center']/div[1]")
    mic_Button_click = (By.XPATH, "//div[@class='button-group-center']/div[1]/div/div")
    screenShare_Button = (By.XPATH, "//div[@class='button-group-center']/div[4]/div/div")
    recording_Button = (By.XPATH, "//div[@class='button-group-center']/div[4]")
    leave_Button = (By.XPATH, "//div[@class='button-group-center']/div[5]")
    edioLogo = (By.XPATH, "(//div[@class='watermark leftwatermark'])[1]")
    ok_Button=(By.XPATH, "//button[text()='Ok']")
    ok_Button1=(By.XPATH, "(//div[@class='button-group'])[1]/button")
    notification_tab=(By.XPATH, "(//div[@class='popup_content'])[1]")
    notification_tab_header = (By.XPATH, "(//div[@class='popup_content'])[1]/h2")
    notification_tab_paragraph = (By.XPATH, "(//div[@class='popup_content'])[1]/p")

    #Side Panel Elements
    sidePanel_camera = (By.XPATH, "//div[@class='participant-list participant-moderator-list']/div[1]/div[2]/em[2]")
    sidePanel_mic = (By.XPATH, "//div[@class='participant-list participant-moderator-list']/div[1]/div[2]/em[1]")
    sidePanel_Header=(By.XPATH, "//div[@class='side-panel']/div[1]")
   # sidePanel_ChatsTab=(By.XPATH, "//a[text()='Chat']")
    sidePanel_ChatsTab = (By.XPATH, "//li[@class='participantTab ']")
    sidePanel_ParticipantTab=(By.XPATH, "//div[@class='participent-tab']/ul/li[2]")
    sidePanel_HostName=(By.XPATH, "//div[@class='presenter-section']")
    chatArea=(By.XPATH, "//div[@id='chat-input']/div/div[2]/textarea")
    #chatArea_Student=(By.XPATH, "//textarea[@id='usermsg']")
    chatArea_Student=(By.XPATH, "//div[@class='usrmsg-form']/textarea")

    sidePanel_micStudent=(By.XPATH, "//div[@class='profile-state']/em[1]")
    sidePanel_cameraStudent=(By.XPATH, "//div[@class='profile-state']/em[2]")
    actions_button=(By.XPATH, "//a[text()='Permissions']")
    settings_button=(By.XPATH, "(//button[@class='button -action -sm default toggle-btn'])[2]")
    sidePanel_rejectRequest=(By.XPATH, "//button[@class='button button-circle -small']/em/span")

    exit_button_Settings=(By.XPATH,"//div[@class='Content__DefaultWrapperComponent-sc-1npw367-0 ffdVFW']/div/h4/div")

    sidePanel_micStudent=(By.XPATH, "//div[@class='participant-list__item']/div[2]/div/em[1]/span")
    sidePanel_cameraStudent = (By.XPATH, "//div[@class='participant-list__item']/div[2]/div/em[2]/span")
    allow_Chat_for_All = (By.XPATH, "(//span[@class='slider round'])[3]")
    disable_chat_for_All = (By.XPATH, "(//span[@class='slider round'])[3]   ")
    allow_mic_for_All = (By.XPATH, "(//span[@class='slider round'])[1]")
    disable_mic_for_All = (By.XPATH, "(//span[@class='slider round'])[1]")

    allow_mic_for_All_Slider=(By.XPATH,"(//label[@class='switch'])[1]/span")
    allow_camera_for_All_Slider = (By.XPATH, "(//label[@class='switch'])[2]/span")

    allow_camera_for_All = (By.XPATH, "(//label[@class='switch'])[2]/span")
    disable_camera_for_All = (By.XPATH, "(//label[@class='switch'])[2]/span")

    allow_Button=(By.XPATH, "//div[@class='participant__request-item ']/div/button[1]")
###############################################################################################

    chat_for_all_slider=(By.XPATH,"(//label[@class ='switch'])[3]/span")


###############################################################################################
    three_dots_Button=(By.XPATH, "(//button[@class='button button-circle participant__action-button'])[1]")
    allow_mic_Button=(By.XPATH, "//button[text()='Allow Mic']")
    disable_mic_Button=(By.XPATH, "//button[text()='Disable Mic']")
    allow_video_Button = (By.XPATH, "//button[text()='Allow Video']")
    disable_video_Button = (By.XPATH, "//button[text()='Disable Video']")
    disable_chat_Button = (By.XPATH, "//button[text()='Disable Chat']")
    allow_chat_Button = (By.XPATH, "//button[text()='Allow Chat']")
    participant_name=(By.XPATH, "(//div[@class='participant__profile'])[1]")
    accept_All_Button=(By.XPATH,"//button[text()='Accept All']")
    clear_All_Button=(By.XPATH,"//button[text()='Clear All']")
    request_count=(By.XPATH, "//div[@class='request-info']/p")
    search_bar_input=(By.XPATH,"//input[@placeholder='Find a participant']")
    search_bar_result=(By.XPATH, "//div[@class='participant-info']/p")
    search_bar_zero_result=(By.XPATH,"//div[@class='participant__scroll-container']/div[2]/div/div")

    three_dots_chat_Button=(By.XPATH, "//button[@class='button button-default  -small']")
    clear_chat_history_Button=(By.XPATH,"//button[text()='Clear chat history']")
    filtered_chat_messages=(By.XPATH, "//div[@class='chatmessage  filtered']")
    objectionable_content_popUp=(By.XPATH,"//div[@class='sc-ifAKCX jQqwMX']")
    objectionable_content_popUp_button=(By.XPATH,"//span[@class='sc-bdVaJa bvRRLh']")
    objectionable_content_popUp_header=(By.XPATH,"//span[@class='sc-gzVnrw hiFilV']")
    objectionable_content_popUp_body = (By.XPATH, "//div[@data-testid='notify.profanityWarningDescription']")
    objectionable_content_popUp_OK=(By.XPATH,"//span[text()='OK']")
    sidePanel_close_button=(By.XPATH, "//span[text()='close']")
    sidePanel_notification_count=(By.XPATH,"//div[@class='participent-tab']ul/li[1]/a/span[2]/span")
    sidePanel_open_button = (By.XPATH, "(//button[@class='button -action -sm default toggle-sidepanel'])[2]")
    sidePanel_chat_notification_count=(By.XPATH,"//div[@class='side-panel-open-button']/div[2]/span/span")

    more_actions_button=(By.XPATH, "//div[@class='button-group-right']/div[2]/div/div/div")
    more_actions_button_Student=(By.XPATH, "//div[@class='toolbox-button-wth-dialog']/div/div/div[@class='toolbox-icon ']")
    manage_vide_quality_button=(By.XPATH, "//span[text()='Manage video quality']")
    share_a_youtube_video_button = (By.XPATH, "//span[text()='Share a YouTube video']")
    view_fullscreen_button = (By.XPATH, "//span[text()='View full screen']")
    exit_fullscreen_button=(By.XPATH, "//span[text()='Exit full screen']")
    settings_button = (By.XPATH, "//span[text()='Settings']")

    speaker_stats_button = (By.XPATH, "//span[text()='Speaker stats']")
    open_shared_document_button = (By.XPATH, "//span[text()='Open shared document']")

    video_quality_dialogBox=(By.XPATH, "//div[@class='Modal__Dialog-sc-1jmnqyd-3 izxwZe']")
    video_quality_dialogBox_done_button = (By.XPATH, "//div[@class='Content__DefaultWrapperComponent-sc-1npw367-0 ffdVFW']/div[3]/div")
    settings_OK_button = (By.XPATH, "//div[@class='Content__DefaultWrapperComponent-sc-1npw367-0 ffdVFW']/div[3]/div/div[2]")
    speaker_stats_close_button = (By.XPATH, "//div[@class='Content__Footer-sc-1npw367-9 ckHUfF Content__HeaderOrFooter-sc-1npw367-2 cUbJrc']/div")

    three_dots_specific_message=(By.XPATH,"//div[@class='chat-message-group remote']/div[1]/div/div[2]/div[2]/button")
    delete_message_button=(By.XPATH, "(//button[text()='Delete Message'])[1]")
    chat_notification_count=(By.XPATH, "//li[@class='participantTab ']/a/span/span")

    unread_notification_count_sidepanel_closed=(By.XPATH,"//div[@class='UnreadMsg ']/span")
    ##########################################################################################

    #On the settings pop-up, on top right there is a cross icon
    def click_exit_button_Settings(self):
      #  self.clickByJavaScript("//div[@class='Content__DefaultWrapperComponent-sc-1npw367-0 ffdVFW']/div/h4/div")
        time.sleep(3)

        ele1 = self.driver.find_element_by_xpath(
        "//div[@class='Content__DefaultWrapperComponent-sc-1npw367-0 ffdVFW']/div[1]/h4/div")
        self.driver.execute_script("arguments[0].click();", ele1)

    #Used to validate the notification count when side panel is closed
    def validate_unread_notification_count(self,expectedCount):
        count=self.driver.find_element(*Jitsi.unread_notification_count_sidepanel_closed).text
        print(count)
        assert count == expectedCount

    #Used to validate notification count of chat messages when focus is on participant tab
    def validate_chat_notification_count(self,expectedCount):

        count=len(self.driver.find_elements_by_xpath("//div[@class='chat-message-group remote']/div"))
        print(count)
        assert count == expectedCount

    #Validate if the chat notification is visible or not
    def notification_visibility(self):
        if self.driver.find_element(*Jitsi.chat_notification_count).is_displayed() :
            print("Element visible")
        else :
            print("Element not visible")
        #print(value)
        #assert value == expectedValue


    def delete_specific_message(self):
        count = self.driver.find_elements_by_xpath("//div[@class='chat-message-group remote']/div")
        print(len(count))
        self.click(self.three_dots_specific_message)
        self.click(self.delete_message_button)

    def click_and_validate_video_quality_box(self):
        self.click(self.manage_vide_quality_button)
        self.highLight("//div[@class='Modal__Dialog-sc-1jmnqyd-3 izxwZe']")
        self.highLight("//div[@class='Content__DefaultWrapperComponent-sc-1npw367-0 ffdVFW']/div[1]")
        self.highLight("//div[@class='Content__DefaultWrapperComponent-sc-1npw367-0 ffdVFW']/div[2]")
        self.highLight("//div[@class='Content__DefaultWrapperComponent-sc-1npw367-0 ffdVFW']/div[3]/div")
        self.click(self.video_quality_dialogBox_done_button)

    def click_more_actions_button(self):
        self.click(self.more_actions_button)


    def click_more_actions_button_Student(self):
        self.click(self.more_actions_button_Student)

    def click_and_validate_fullscreen_button(self):
        self.click(self.view_fullscreen_button)
        self.click(self.more_actions_button)
        time.sleep(3)
        self.highLight("//span[text()='Exit full screen']")
        time.sleep(2)
        self.click(self.exit_fullscreen_button)

    def click_and_validate_settings_button(self):
        self.click(self.settings_button)
        self.highLight("//div[@class='Modal__Dialog-sc-1jmnqyd-3 izxwZe']")
        self.highLight("//div[@class='Content__DefaultWrapperComponent-sc-1npw367-0 ffdVFW']/div[1]")
        self.highLight("//div[@class='Content__DefaultWrapperComponent-sc-1npw367-0 ffdVFW']/div[2]")
        self.highLight("//div[@class='Content__DefaultWrapperComponent-sc-1npw367-0 ffdVFW']/div[3]")
        self.click(self.settings_OK_button)

    def validate_options_under_more_action_button(self):
        # self.highLight("(//div[@class='toolbox-icon '])[2]/div")
        self.highLight("//span[text()='Manage video quality']")
        # self.highLight("//span[text()='Share a YouTube video']")
        self.highLight("//span[text()='View full screen']")
        self.highLight("//span[text()='Settings']")
        # self.highLight("//span[text()='Speaker stats']")
        # self.highLight("//span[text()='Open shared document']")

    def click_and_validate_speaker_stats(self):
        self.click(self.speaker_stats_button)
        self.highLight("//div[@class='Modal__Dialog-sc-1jmnqyd-3 izxwZe']")
        self.click(self.speaker_stats_close_button)

    def click_close_sidePanel_Button(self):
        self.click(self.sidePanel_close_button)

    def validate_sidePanel_notification_Count(self,expectedCount):
        count=self.driver.find_element(*Jitsi.sidePanel_notification_count).text
        assert count == expectedCount

    def click_open_sidePanel(self):
        self.click(self.sidePanel_open_button)

    def send_chat_message_Student(self,text):
        #self.driver.find_element(*Jitsi.chatArea_Student).click()
        #self.click(self.chatArea_Student)
        self.clickByJavaScript("//textarea[@id='usermsg']")
        self.driver.find_element(*Jitsi.chatArea_Student).send_keys(text)
        self.driver.find_element(*Jitsi.chatArea_Student).send_keys(Keys.ENTER)

    def validate_Objectionable_Content_PopUp(self):
        self.highLight("//div[@class='sc-ifAKCX jQqwMX']")
        self.click(self.objectionable_content_popUp_button)
        header =self.driver.find_element(*Jitsi.objectionable_content_popUp_header).text
        assert header == "Profanity Warning"

        body=self.driver.find_element(*Jitsi.objectionable_content_popUp_body).text
        print(body)
        #assert body.replace(" ","") == "Yourmessagecannotbepostedas-issinceitcontainsobjectionablelanguage.Pleasereviseandrepostyourmessage"
        assert body == "Your message cannot be posted as-is since it contains objectionable language. Please revise and repost your message"

        self.highLight("//span[text()='OK']")
        self.click(self.objectionable_content_popUp_OK)


    def click_three_dots_chat_Button(self):
        self.click(self.three_dots_chat_Button)

    def click_clearChat_history_Button(self):
        #self.click(self.clear_chat_history_Button)
        self.clickByJavaScript("//button[text()='Clear chat history']")

    def validate_filtered_chat_messages(self,expectedCount):
        listOfElements=self.driver.find_elements(*Jitsi.filtered_chat_messages)
        countofMessages=len(listOfElements)
        print(countofMessages)
        #assert countofMessages == expectedCount

    def validate_blank_chatArea(self):
        list=self.driver.find_elements_by_xpath("//div[@id='chatconversation']/div/div")
        count=len(list)
        print(count)
        assert count == 0

    def collect_chat_messages(self,deletedMessage):
        list=self.driver.find_elements_by_xpath("//div[@class='chat-message-group local']/div/div")
        count=len(list)
        print(count)

        for x in list:
            message=x.find_element_by_xpath("div[2]/div/div/div").text
            print(message)
            if message == deletedMessage:
                pass
            else :
                print("deleted message visible in chat window")
                assert 1 == 2
                break

    def collect_chat_messages1(self,deletedMessage):
        list=self.driver.find_elements_by_xpath("//div[@class='chat-message-group local']/div/div")
        count=len(list)
        print(count)

        for x in list:
            message=x.find_element_by_xpath("div[2]/div/div/div").text
            print(message)
            if message == deletedMessage:
                pass
            else :
                print("deleted message visible in chat window")
                assert 1 == 2
                break


    def enter_search_input(self,inputName):
        self.driver.find_element(*Jitsi.search_bar_input).clear()
        self.send_value(self.search_bar_input,inputName)
        self.driver.find_element(*Jitsi.search_bar_input).send_keys(Keys.ENTER)

    def validate_zero_searchBar_results(self):
        time.sleep(5)
        elementsList=self.driver.find_elements(*Jitsi.search_bar_zero_result)
        print(len(elementsList))
        assert len(elementsList) == 0

    def validate_searchbar_result(self,expecteValue):
       # self.waitForElementVisibility_Xpath("//div[@class='participant-info']/p")
        time.sleep(10)
        actualValue=self.driver.find_element(*Jitsi.search_bar_result).text
        print("Actual value :"+actualValue)
        if expecteValue in actualValue:
            print("Value found")
        else:
            assert 1== 0


    def calculate_requestCount(self,expectedCount):
        count=self.driver.find_element(*Jitsi.request_count).text
        print(count)
        assert count == expectedCount+" Requests"

    def clickChatButton(self):
        time.sleep(3)
        self.driver.find_element(*Jitsi.sidePanel_ChatsTab).click()
        time.sleep(3)

    def validate_chatArea_Visibility(self):
        value = self.is_visible(self.chatArea)
        print(value)

    def sendChatMessages(self,text):
        self.driver.find_element(*Jitsi.chatArea).send_keys(text)
        self.driver.find_element(*Jitsi.chatArea).send_keys(Keys.ENTER)

    def getMessagesCount(self,expectedCount):
        count=self.driver.find_elements_by_xpath("//div[@class='chat-message-group remote']/div")
        print(len(count))
      #  assert expectedCount == count



    #Testing
    def clickOkButton1(self):
        time.sleep(5)
        iframe = self.driver.find_element_by_name('jitsiConferenceFrame0')
        self.driver.switch_to.frame(iframe)
        print("Inside new window")
        self.driver.find_element(*Jitsi.ok_Button1).click()
        time.sleep(5)




    #Click on MORE OPtion from side panel
    def clickMoreOption(self):
        time.sleep(3)
        self.driver.find_element(*Jitsi.sidePanel_moreOption).click()


    def clickAllowOption(self):
        time.sleep(3)
        self.driver.find_element(*Jitsi.allow_Chat_for_All).click()

    def clickDisableOption(self):
        time.sleep(3)
        self.driver.find_element(*Jitsi.disable_chat_for_All).click()



    def clickParticipantTab(self):
        time.sleep(2)
        self.driver.find_element(*Jitsi.sidePanel_ParticipantTab).click()

    def switch(self):
        self.driver.switch_to_window(self.driver.window_handles[1])

    def clickJoinMeeting2(self):
        time.sleep(20)
        print("Inside new window")
        iframe = self.driver.find_element_by_xpath("//div[@id='jitsiContainer']/iframe")
        self.driver.switch_to.frame(iframe)
        self.waitForElementVisibility_Xpath("//button[@class='button-prejoin sc-chPdSV dKdtrl']")
        self.driver.find_element(*Jitsi.joinMeeting_button).click()
        print("Clicked")



    # self.waitForElementVisibility_Xpath("\\h1")

        self.driver.find_element(*Jitsi.joinMeeting_button).click()
        print("Clicked")




    #Validate Teacher's default status of camera on entering classroom
    @allure.step("Successfully validated the default state of camera on entering the session")
    def validateCameraStateOfTeacher(self):
        self.highLight("//div[@class='button-group-center']/div[1]")
        # Capturing mic's attribute value before enabling it
        disabledCamera = self.driver.find_element(*Jitsi.camera_Button)
        attributeValue = disabledCamera.get_attribute('aria-pressed')
        print(attributeValue)
        time.sleep(1)
        assert attributeValue == "true"

        # capturing camera status from side panel before enabling it
        cameraSidePanelStatus = self.driver.find_element(*Jitsi.sidePanel_camera).text
        print("Camera status: " + cameraSidePanelStatus)
        assert cameraSidePanelStatus == "videocam_off"

    # Validate Teacher's default status of mic on entering classroom
    @allure.step("Successfully validated the default state of mic on entering the session")
    def validateMicStateOfTeacher(self):
        self.highLight("//div[@class='button-group-center']/div[2]")
        # Capturing mic's attribute value before enabling it
        disabledMic = self.driver.find_element(*Jitsi.mic_Button)
        attributeValue = disabledMic.get_attribute('aria-pressed')
        print(attributeValue)
        time.sleep(1)
        assert attributeValue == "true"

        # capturing mic status from side panel before enabling it
        micSidePanelStatus = self.driver.find_element(*Jitsi.sidePanel_mic).text
        print("Camera status: " + micSidePanelStatus)
        assert micSidePanelStatus == "mic_off"



    #Checking Student's default state of camera on entering classroom
    def validateDefaultStateOfCamera(self):

        time.sleep(5)
       # self.waitForElementVisibility_Xpath("//div[@class='button-group-center']/div[1]")
        self.highLight("//div[@class='button-group-center']/div[1]")
        # Capturing mic's attribute value before enabling it
        disabledCamera = self.driver.find_element(*Jitsi.camera_Button)
        attributeValue = disabledCamera.get_attribute('aria-pressed')
        print(attributeValue)
        time.sleep(5)
        assert attributeValue == "true"

        # capturing camera status from side panel before enabling it
        cameraSidePanelStatus = self.driver.find_element(*Jitsi.sidePanel_cameraStudent).text
        print("Camera status: " + cameraSidePanelStatus)
        assert cameraSidePanelStatus == "videocam_off"

    # Checking Student's default state of mic on entering classroom
    def validateDefaultStateOfMic(self):
        time.sleep(5)
        self.highLight("//div[@class='button-group-center']/div[2]")
        # Capturing mic's attribute value before enabling it
        disabledMic = self.driver.find_element(*Jitsi.mic_Button)
        attributeValue = disabledMic.get_attribute('aria-pressed')
        print(attributeValue)
        time.sleep(1)
        assert attributeValue == "true"

        # capturing mic status from side panel before enabling it
        self.highLight("//div[@class='participant-list__item']/div[2]/div/em[1]/span")
        micSidePanelStatus = self.driver.find_element(*Jitsi.sidePanel_micStudent).text
        print("Mic status: " + micSidePanelStatus)
        assert micSidePanelStatus == "mic_off"

    #Not sure if used anywhere
    def clickCamera(self):
        time.sleep(5)
        iframe = self.driver.find_element_by_name('jitsiConferenceFrame0')
        self.driver.switch_to.frame(iframe)
        print("Inside new window")
        self.driver.find_element(*Jitsi.camera1).click()
        time.sleep(10)


    def clickCamera1(self):
        time.sleep(5)
        self.driver.find_element(*Jitsi.camera1).click()
        time.sleep(10)







        # Click Mic and check state of mic for teacher








    # Method to click on Screenshare button to share the user screen
    def click_screenShare_button_Student(self):

        # Capturing mic's attribute value before enabling it
       # disabledScreen = self.driver.find_element(*Jitsi.screenShare_Button)
       # ScreenshareStatusBeforeEnabling = disabledScreen.get_attribute('aria-pressed')
       # print(ScreenshareStatusBeforeEnabling)
        #assert attributeValue == "false"
        self.click(self.screenShare_Button)

       # enabledScreen = self.driver.find_element(*Jitsi.screenShare_Button)
       # screenShareAfterEnabling = enabledScreen.get_attribute('aria-pressed')
       # print(screenShareAfterEnabling)

       # while screenShareAfterEnabling == ScreenshareStatusBeforeEnabling :
       #     assert 1==2

    # Method to click on Recording button to start/stop recording
    def clickRecordingButton(self):
        self.driver.find_element(*Jitsi.recording_Button).click()



    #Validate default chat state for Students. Validate if the chat area is visible or not
    def validateChatState(self):
        time.sleep(5)
        self.highLight("//div[@class='side-panel']/div[2]/ul/li[1]/a")
        self.clickByJavaScript("//a[text()='Chat']")
        time.sleep(3)
        chatState=self.driver.find_elements(*Jitsi.chatArea)
      #  print("Chat State :"+chatState)
       # assert chatState.is_displayed() == True
        s=len(chatState)
        assert s == 0
        self.clickByJavaScript("//a[text()='Participants']")



    def switchIframe(self):
        time.sleep(3)
        iframe = self.driver.find_element_by_name('jitsiConferenceFrame0')
        self.driver.switch_to.frame(iframe)

    ###############################################################################


    #This is an individual action used to allow mic for specific student from side panel
    def click_allow_mic_Button(self):
        self.click(self.allow_mic_Button)

    # This is an individual action used to allow mic for specific student from side panel
    def click_disable_mic_Button(self):
        self.click(self.disable_mic_Button)

    # This is an individual action used to allow chat for specific student from side panel
    def click_allow_chat_Button(self):
        time.sleep(2)
        self.click(self.allow_chat_Button)

    # This is an individual action used to disallow chat for specific student from side panel
    def click_disable_chat_Button(self):
        self.click(self.disable_chat_Button)

    # This is an individual action used to allow video for specific student from side panel
    def click_allow_video_Button(self):
        self.click(self.allow_video_Button)

    # This is an individual action used to allow mic for specific student from side panel
    def click_disable_video_Button(self):
        self.click(self.disable_video_Button)

    # This is an individual action used to allow mic for specific student from side panel
    def click_ThreeDots_button(self):
        #self.hover()
        self.click(self.three_dots_Button)
        #self.clickByJavaScript(self.three_dots_Button)

    @allure.step("Clicked on Join meeting button")
    def click_joinMeeting_button(self):

        time.sleep(20)
        iframe = self.driver.find_element_by_name('jitsiConferenceFrame0')
        self.driver.switch_to.frame(iframe)
        print("Inside new window")
        self.click(self.joinMeeting_button)

    @allure.step("Default state of Teacher's session is as expectation.")
    def validate_default_state_of_teacher(self):
        time.sleep(5)
        self.validate_default_camera_setting_of_teacher()
        self.validate_default_mic_setting_of_teacher()

        # Validate Teacher's default status of camera on entering classroom

    @allure.step("Successfully validated the default state of camera on entering the session")
    def validate_default_camera_setting_of_teacher(self):
        self.highLight("//div[@class='button-group-center']/div[1]")
        # Capturing mic's attribute value before enabling it
        disabledCamera = self.driver.find_element(*Jitsi.camera_Button)
        attributeValue = disabledCamera.get_attribute('aria-pressed')
        print(attributeValue)
        time.sleep(1)
        assert attributeValue == "true"

        # capturing camera status from side panel before enabling it
        cameraSidePanelStatus = self.driver.find_element(*Jitsi.sidePanel_camera).text
        print("Camera status: " + cameraSidePanelStatus)
        assert cameraSidePanelStatus == "videocam_off"

    # Validate Teacher's default status of mic on entering classroom


    def validate_default_mic_setting_of_teacher(self):
        self.highLight("//div[@class='button-group-center']/div[2]")
        # Capturing mic's attribute value before enabling it
        disabledMic = self.driver.find_element(*Jitsi.mic_Button)
        attributeValue = disabledMic.get_attribute('aria-pressed')
        print(attributeValue)
        time.sleep(1)
        assert attributeValue == "true"

        # capturing mic status from side panel before enabling it
        micSidePanelStatus = self.driver.find_element(*Jitsi.sidePanel_mic).text
        print("Camera status: " + micSidePanelStatus)
        assert micSidePanelStatus == "mic_off"

    # Verifying the visibility of elements in the classroom
    @allure.step("Successfully validated the various elements from side panel")
    def validate_visibility_Of_sidePanel_elements_teacher(self):
        # Check if the side panel is open
        self.highLight("//div[@class='side-panel']")

        # Check if the header is visible
        self.highLight("//div[@class='side-panel']/div[1]")

        # Check if the Chat Tab is visible
        self.highLight("//div[@class='side-panel']/div[2]/ul/li[1]")
        chatText = self.driver.find_element(*Jitsi.sidePanel_ChatsTab).text
        print("Chats Text :" + chatText)
        # assert chatText == "Chat"

        # Check if the Participant Tab is visible
        self.highLight("//div[@class='side-panel']/div[2]/ul/li[2]")
        participantText = self.driver.find_element(*Jitsi.sidePanel_ParticipantTab).text
        print("Participant Text :" + participantText)
        # assert chatText == "Participants"

        # Check if the HOST Label is visible
        self.highLight("//div[@class='presenter-section']")
        # hostname=self.driver.find_element(*Jitsi.sidePanel_HostName).find_element_by_xpath("div/h3").text
        # assert hostname == "HOST"

        # Check if the avatar beside name is visible
        self.highLight("//div[@class='participant-list participant-moderator-list']/div/div[1]/div[1]")

        # Check if the host name is visible
        self.highLight("//div[@class='participant-list participant-moderator-list']/div/div[1]/div[2]")

        # Check if the Participant list column is visible
        self.highLight("//div[@class='participant-list']")
        # write logic to check spelling

        # Check if the MORE button is visible
        #self.highLight("//button[@class='button button-icon -small']")

        # Check if the Search Bar is visible
        self.highLight("//input[@placeholder='Find a participant']")

        # Check if the host name is visible
        self.highLight("//div[@class='participant-list participant-moderator-list']/div/div[1]/div[2]")

    # Click Camera and check state of camera
    @allure.step("Clicked on camera button.")
    def click_camera_button(self):

        time.sleep(5)
        # capturing state of camera before clicking on it
        cameraStateBeforeClick = self.driver.find_element(*Jitsi.camera_Button).get_attribute('aria-pressed')
        print(cameraStateBeforeClick)

        # capturing camera status from side panel before clicking on it
        cameraSidePanelStatusBeforeClick = self.driver.find_element(*Jitsi.sidePanel_camera).text
        print(cameraSidePanelStatusBeforeClick)

        self.highLight("//div[@class='button-group-center']/div[1]")
        #self.driver.find_element(*Jitsi.camera_Button).click()
        self.click(self.camera_Button)
        time.sleep(10)

        # capturing state of camera after clicking on it
        cameraStateAfterClick = self.driver.find_element(*Jitsi.camera_Button).get_attribute('aria-pressed')
        print(cameraStateAfterClick)

        # capturing camera status from side panel after clicking on it
        cameraSidePanelStatusAfterClick = self.driver.find_element(*Jitsi.sidePanel_camera).text
        print(cameraSidePanelStatusAfterClick)

        while cameraStateBeforeClick == cameraStateAfterClick or cameraSidePanelStatusBeforeClick == cameraSidePanelStatusAfterClick:
            assert 1 == 2

    #Click Mic and check state of mic for teacher
    def click_mic_button(self):

        time.sleep(5)
        #capturing state of mic before clicking on it
        micStateBeforeClick=self.driver.find_element(*Jitsi.mic_Button).get_attribute('aria-pressed')
        print(micStateBeforeClick)

        # capturing mic status from side panel before clicking on it
        micSidePanelStatusBeforeClick = self.driver.find_element(*Jitsi.sidePanel_mic).text
        print(micSidePanelStatusBeforeClick)

        self.highLight("//div[@class='button-group-center']/div[2]")
       # self.driver.find_element(*Jitsi.mic_Button).click()
        self.click(self.mic_Button)
        time.sleep(10)

        # capturing state of mic after clicking on it
        micStateAfterClick = self.driver.find_element(*Jitsi.mic_Button).get_attribute('aria-pressed')
        print(micStateAfterClick)

        # capturing mic status from side panel after clicking on it
        micSidePanelStatusAfterClick = self.driver.find_element(*Jitsi.sidePanel_mic).text
        print(micSidePanelStatusAfterClick)

        while micStateBeforeClick == micStateAfterClick or micSidePanelStatusBeforeClick == micSidePanelStatusAfterClick:
            assert 1==2

    def validate_default_state_of_student(self):
        self.validate_default_mic_setting_of_Student()
        self.validate_default_camera_setting_of_Student()
      #  self.validateChatState()

    #Checking Student's default state of camera on entering classroom
    def validate_default_camera_setting_of_Student(self):

        time.sleep(5)
       # self.waitForElementVisibility_Xpath("//div[@class='button-group-center']/div[1]")
        self.highLight("//div[@class='button-group-center']/div[1]")
        # Capturing mic's attribute value before enabling it
        disabledCamera = self.driver.find_element(*Jitsi.camera_Button)
        attributeValue = disabledCamera.get_attribute('aria-pressed')
        print(attributeValue)
        time.sleep(5)
        assert attributeValue == "true"

        # capturing camera status from side panel before enabling it
        cameraSidePanelStatus = self.driver.find_element(*Jitsi.sidePanel_cameraStudent).text
        print("Camera status: " + cameraSidePanelStatus)
        assert cameraSidePanelStatus == "videocam_off"

    # Checking Student's default state of mic on entering classroom
    def validate_default_mic_setting_of_Student(self):
        time.sleep(5)
        self.highLight("//div[@class='button-group-center']/div[2]")
        # Capturing mic's attribute value before enabling it
        disabledMic = self.driver.find_element(*Jitsi.mic_Button)
        attributeValue = disabledMic.get_attribute('aria-pressed')
        print(attributeValue)
        time.sleep(1)
        assert attributeValue == "true"

        # capturing mic status from side panel before enabling it
        self.highLight("//div[@class='participant-list__item']/div[2]/div/em[1]/span")
        micSidePanelStatus = self.driver.find_element(*Jitsi.sidePanel_micStudent).text
        print("Mic status: " + micSidePanelStatus)
        assert micSidePanelStatus == "mic_off"

    def validate_notification_tab_content(self,expectedValue):
        header=self.driver.find_element(*Jitsi.notification_tab_header).text
        print(header)
        assert header == "You have requested to enable the " + expectedValue

        paragraph=self.driver.find_element(*Jitsi.notification_tab_paragraph).text
        assert paragraph == "Click Ok to continue or you can Cancel your request"

    def validate_notification_tab_content_screenshare(self):
        header=self.driver.find_element(*Jitsi.notification_tab_header).text
        print(header)
        assert header == "You have requested to share your screen"

        paragraph=self.driver.find_element(*Jitsi.notification_tab_paragraph).text
        assert paragraph == "Click Ok to continue or you can Cancel your request"

    #Request access for mic as a Student
    def request_mic_access(self):
        self.click(self.mic_Button_click)
        self.highLight("(//div[@class='popup_content'])[1]")
        self.validate_notification_tab_content("microphone")
        self.click(self.ok_Button)


        # Request access for mic as a Student
    def request_screenShare_access(self):
        self.click(self.screenShare_Button)
        self.highLight("(//div[@class='popup_content'])[1]")
        self.validate_notification_tab_content_screenshare()
        self.click(self.ok_Button)


    def validate_requests_count(self,expectedCount):
        requestCount=self.driver.find_elements_by_xpath("//div[@class='participant__scroll-container']/div[1]/div[3]/div")
        assert len(requestCount) == expectedCount

    #Method to click on 'Allow' button from side panel
    def click_allow_button(self):
        time.sleep(5)
        iframe = self.driver.find_element_by_name('jitsiConferenceFrame0')
        self.driver.switch_to.frame(iframe)
        print("Inside new window")
        self.click(self.allow_Button)

    # Method to click on 'OK' button
    def click_ok_button(self):
        time.sleep(5)
        ele = self.driver.find_element_by_xpath("//div[@class='popup_overlay show-notification']/div/div/button")
        self.driver.execute_script("arguments[0].click();", ele)

    def click_ok_button1(self):
        self.click(self.ok_Button)

    # Click Mic and check state of mic for Student
    def click_mic_button_Student(self):

        time.sleep(5)
        # capturing state of mic before clicking on it
        micStateBeforeClick = self.driver.find_element(*Jitsi.mic_Button).get_attribute('aria-pressed')
        print(micStateBeforeClick)

        # capturing mic status from side panel before clicking on it
        micSidePanelStatusBeforeClick = self.driver.find_element(*Jitsi.sidePanel_micStudent).text
        print(micSidePanelStatusBeforeClick)

        self.highLight("//div[@class='button-group-center']/div[2]")
        #self.driver.find_element(*Jitsi.mic_Button).click()
        self.click(self.mic_Button)
        time.sleep(3)

        # capturing state of mic after clicking on it
        micStateAfterClick = self.driver.find_element(*Jitsi.mic_Button).get_attribute('aria-pressed')
        print(micStateAfterClick)

        # capturing mic status from side panel after clicking on it
        micSidePanelStatusAfterClick = self.driver.find_element(*Jitsi.sidePanel_micStudent).text
        print(micSidePanelStatusAfterClick)

        while micStateBeforeClick == micStateAfterClick or micSidePanelStatusBeforeClick == micSidePanelStatusAfterClick:
            assert 1 == 2

    #Method to click on Leave button to exit room
    def click_exit_button(self):
        self.click(self.leave_Button)

    # reject request raised
    def reject_mic_access(self):
        time.sleep(5)
        iframe = self.driver.find_element_by_name('jitsiConferenceFrame0')
        self.driver.switch_to.frame(iframe)
        self.highLight("//button[@class='button button-circle -small']/em/span")
        self.click(self.sidePanel_rejectRequest)

    def reject_camera_access(self):
        time.sleep(5)
        self.driver.find_element_by_xpath("//div[@class='participant__request']/div/div/button[2]/em/span").click()


    def request_camera_access(self):
        self.highLight("//div[@class='button-group-center']/div[2]")
       # self.driver.find_element(*Jitsi.camera_Button).click()
        self.click(self.camera_Button)
        self.highLight("(//div[@class='popup_content'])[1]")
        self.validate_notification_tab_content("camera")
        self.click(self.ok_Button)
        #self.driver.find_element(*Jitsi.ok_Button).click()


    # Click camera and check state of mic for student
    def click_camera_button_Student(self):

        time.sleep(5)
        # capturing state of camera before clicking on it
        cameraStateBeforeClick = self.driver.find_element(*Jitsi.camera_Button).get_attribute('aria-pressed')
        print(cameraStateBeforeClick)

        # capturing camera status from side panel before clicking on it
        cameraSidePanelStatusBeforeClick = self.driver.find_element(*Jitsi.sidePanel_cameraStudent).text
        print(cameraSidePanelStatusBeforeClick)

        self.highLight("//div[@class='button-group-center']/div[1]")
       # self.driver.find_element(*Jitsi.camera_Button).click()
        self.click(self.camera_Button)
        time.sleep(5)

        # capturing state of camera after clicking on it
        cameraStateAfterClick = self.driver.find_element(*Jitsi.camera_Button).get_attribute('aria-pressed')
        print(cameraStateAfterClick)

        # capturing camera status from side panel after clicking on it
        cameraSidePanelStatusAfterClick = self.driver.find_element(*Jitsi.sidePanel_cameraStudent).text
        print(cameraSidePanelStatusAfterClick)

        while cameraStateBeforeClick == cameraStateAfterClick or cameraSidePanelStatusBeforeClick == cameraSidePanelStatusAfterClick:
           # assert 1 == 2
           print("Inside assert function")
           pass

    def switch_Iframe(self):
        iframe = self.driver.find_element_by_name('jitsiConferenceFrame0')
        self.driver.switch_to.frame(iframe)


    #Click on MORE OPtion from side panel
    def click_actions_button(self):
        self.click(self.actions_button)


    # Click on Settings button from bottom panel
    def click_settings_button(self):
        #self.click(self.settings_button)
        self.driver.find_element_by_xpath("//div[@class='button-group-right']/div[1]/div/button").click()

    def click_allow_option_Mic(self):
        self.click(self.allow_mic_for_All)

    def click_enable_Mic_slider(self):
        time.sleep(2)
        self.click(self.allow_mic_for_All_Slider)

    def click_allow_option_Camera(self):
        self.click(self.allow_camera_for_All)
       #self.clickByJavaScript("//span[@class='slider round'])[2]")

    def click_enable_camera_Slider(self):
        self.click(self.allow_camera_for_All_Slider)

    def click_allow_option_Chat(self):
        self.click(self.allow_Chat_for_All)

    def enable_chat_forAll(self):
        self.driver.find_element_by_xpath("//label[@labelfor='stdTechChatSwitch']/span").click()

    def click_disable_option_Chat(self):
        self.click(self.disable_chat_for_All)

    def click_disable_option_Mic(self):
        self.click(self.disable_mic_for_All)


    def click_disable_option_Camera(self):
        self.click(self.disable_camera_for_All)


    def click_chat_button(self):
        self.click(self.sidePanel_ChatsTab)


    def send_chatMessages(self,text):
        self.send_value(self.chatArea,text)
        self.send_value(self.chatArea,Keys.ENTER)


    def get_messages_count(self,expectedCount):
        count=self.driver.find_elements_by_xpath("//div[@class='chat-message-group remote']/div")
        print(len(count))

    def click_participant_button(self):
        self.click(self.sidePanel_ParticipantTab)


    def click_disable_option_Chat(self):
        self.click(self.disable_chat_for_All)

    def click_acceptAll_Button(self):
        self.click(self.accept_All_Button)


    def click_clearAll_Button(self):
        self.click(self.clear_All_Button)





