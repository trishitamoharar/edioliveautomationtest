import time

import allure
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from EdioLive.utilities.BaseClass import BaseClass

from EdioLive.pageObjects.Jitsi import Jitsi


class CoursePage(BaseClass):

    def __init__(self, driver):
        self.driver=driver

    videoURL_Button=(By.XPATH, "//button[@class='c-button -icon c-virtual-classroom-button -success-alternate']")
    launch_Button=(By.XPATH, "//button[@class='c-button -icon c-virtual-classroom-action-content__launch-button']")



    def clickLaunchButton(self):
        self.waitForElementVisibility_Xpath("//button[@class='c-button -icon c-virtual-classroom-action-content__launch-button']")

        window_before = self.driver.window_handles[0]
        print("Before Switching:" +window_before)
        self.driver.find_element(*CoursePage.launch_Button).click()
        window_after = self.driver.window_handles[1]
        time.sleep(10)
        print("After switching :" + window_after)
        self.driver.switch_to.window(window_after)

    @allure.step("Clicked on Launch button")
    def clickLaunchButton1(self):

        global p
        self.waitForElementVisibility_Xpath("//button[@class='c-button -icon c-virtual-classroom-action-content__launch-button']")

        # get current window handle
        p = self.driver.current_window_handle
        self.driver.find_element(*CoursePage.launch_Button).click()

        # get first child window
        child = self.driver.window_handles

        for w in child:
            # switch focus to child window
            if (w != p):
                self.driver.switch_to.window(w)
                print("New Window:" +w)
                break
        time.sleep(30)
        jit=Jitsi(self.driver)
        return jit

    @allure.step("Clicked on Launch button")
    def clickLaunchButton2(self):
        global p
        self.waitForElementVisibility_Xpath(
            "//button[@class='c-button -icon c-virtual-classroom-action-content__launch-button']")

        # get current window handle
        p = self.driver.current_window_handle
        self.driver.find_element(*CoursePage.launch_Button).click()

        # get first child window
        child = self.driver.window_handles

        for w in child:
            # switch focus to child window
            if (w != p):
                self.driver.switch_to.window(w)
                print("New Window:" + w)
                break
        time.sleep(30)
        return p


    #Working
    def clickLaunchButtonTesting(self):

        self.waitForElementVisibility_Xpath("//button[@class='c-button -icon c-virtual-classroom-action-content__launch-button']")
        p=self.getDefaultWindowHandle()
        self.driver.find_element(*CoursePage.launch_Button).click()
        self.switchToNewTab(p)
        time.sleep(30)
        jit = Jitsi(self.driver)
        return jit

    def waitForElementVisibility_Xpath(self, xpath):
        WebDriverWait(self.driver, 30).until(
            EC.presence_of_element_located((By.XPATH, xpath)))

    ###############################################################

    #TEsting
    @allure.step("Clicked on Video link")
    def click_videoLink(self):

        self.click(self.videoURL_Button)


    #Testing
    @allure.step("Clicked on Launch button")
    def click_launch_button(self):

        global p
        self.waitForElementVisibility_Xpath(
            "//button[@class='c-button -icon c-virtual-classroom-action-content__launch-button']")

        # get current window handle
        p = self.driver.current_window_handle
        self.click(self.launch_Button)

        # get first child window
        child = self.driver.window_handles

        for w in child:
            # switch focus to child window
            if (w != p):
                self.driver.switch_to.window(w)
                print("New Window:" + w)
                break
        time.sleep(15)
        jit = Jitsi(self.driver)
        return jit