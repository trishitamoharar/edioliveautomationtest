import time

import allure
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from EdioLive.pageObjects.CoursePage import CoursePage
from EdioLive.utilities.BaseClass import BaseClass
from EdioLive.utilities.config import TestData


class LearningPage(BaseClass):

    def __init__(self, driver):
        self.driver=driver

    #log = BaseClass.getLogger()

    searchBar=(By.XPATH, "//div[@class='o-input']/input")
    searchResult=(By.XPATH, "(//div[@class='c-course-card__content'])[2]")
    goToCourse_Button=(By.XPATH, "(//button[text()='Go to Course'])[1]")
    goToCourse_Button1 = (By.XPATH, "(//button[text()='Go to Course'])[2]")
    goToCourse_Button2 = (By.XPATH, "//div[text()='ELIZABETH TROXELL']/parent::div/parent::div/parent::div/parent::div/div[3]/div/button[1]")
    courseOutline_Button=(By.XPATH, "//a[text()='Course Outline']")
    liveClass_button=(By.XPATH, "//button[@class='c-button -neutral-alternate -rich -secondary']")
    chapter=(By.XPATH, "(//div[@class='c-course-outline__row-wrapper c-course-outline__day -level2'])[1]/div/div[2]/div")
    chapter1=(By.XPATH, "//label[text()='An Introduction To Real Numbers ']")

    chapter2=(By.XPATH, "(//div[@class='c-course-outline__row-contents c-course-outline__row-contents-drag-disabled'])[2]")
    chapter3 = (By.XPATH, "//label[text()='The Scope of Chemistry']")
    chapter4=(By.XPATH, "(//label[text()='Introduction'])[2]")
    chapter5=(By.XPATH, "(//button[@aria-label='Open Node'])[1]")

    progress_Button=(By.XPATH, "//a[text()='Progress']")
    mycourses_dropdown=(By.XPATH, "//button[text()='My Courses']")
    allcourses_dropdown = (By.XPATH, "//a[text()='All Courses']")

    unit_Student=(By.XPATH, "(//div[@class='c-course-outline__row-expand']/button)[1]")
    chapter_Student=(By.XPATH, "(//div[@class='c-course-outline__row'])[2]/div/div/a")


    topic_Student1=(By.XPATH, "(//div[@class='c-course-outline__row-expand'])[2]")



    @allure.step("Entered search value in searchbar")
    def enterValueInSearchBar3(self):
        self.waitForElementVisibility_Xpath("//div[@class='o-input']/input")
        # time.sleep(10)
        self.driver.find_element(*LearningPage.searchBar).send_keys("Science 6")

    @allure.step("Entered search value in searchbar")
    def enterValueInSearchBar2(self):


        self.waitForElementVisibility_Xpath("//div[@class='o-input']/input")
        # time.sleep(10)
        self.driver.find_element(*LearningPage.mycourses_dropdown).click()
        time.sleep(3)
        self.driver.find_element(*LearningPage.allcourses_dropdown).click()
        time.sleep(2)
        self.driver.find_element(*LearningPage.searchBar).send_keys("Science 6")









    @allure.step("Selected search result")
    def selectSearchResult1(self):
        time.sleep(10)

        self.waitForElementVisibility_Xpath("(//button[text()='Go to Course'])[3]")

        self.driver.find_element(*LearningPage.goToCourse_Button1).click()



        self.driver.find_element(*LearningPage.goToCourse_Button).click()

    @allure.step("Clicked on Course Outline button")
    def clickCourseOutlineButton(self):
        self.waitForElementVisibility_Xpath("//a[text()='Course Outline']")
       # time.sleep(10)
        self.driver.find_element(*LearningPage.courseOutline_Button).click()



    @allure.step("Clicked on Unit")
    def clickUnit(self):
        self.waitForElementVisibility_Xpath("//div[@class='rst__tree c-course-outline']/div")
        aList=self.driver.find_elements_by_xpath("//div[@class='rst__tree c-course-outline']/div")
        for a in aList:
            a.find_element_by_xpath("div[2]/div/div/div[1]/button").click()
            #a.find_element_by_xpath("[2]").click()
            break
        time.sleep(5)

    @allure.step("Clicked on Unit")
    def clickUnit1(self):
        self.waitForElementVisibility_Xpath("//div[@class='rst__tree c-course-outline']/div")
        aList = self.driver.find_elements_by_xpath("//div[@class='rst__tree c-course-outline']/div")
        for a in aList:
            a.find_element_by_xpath("div[2]/div/div/div[1]/button").click()
            # a.find_element_by_xpath("[2]").click()
            break


        time.sleep(5)

    @allure.step("Clicked on Chapter")
    def clickChapter(self):
        self.waitForElementVisibility_Xpath("(//div[@class='c-course-outline__row-wrapper c-course-outline__day -level2'])[1]/div/div[2]/div")
        self.driver.find_element(*LearningPage.chapter1).click()
        cp=CoursePage(self.driver)
        return cp
       # self.clickByJavaScript("(//div[@class='c-course-outline__row-wrapper c-course-outline__day -level2'])[1]/div/div[2]/div")
        #time.sleep(3)
        #self.driver.find_element(*LearningPage.chapter).click()

    @allure.step("Clicked on Chapter")
    def clickChapter2(self):
        self.waitForElementVisibility_Xpath("//label[text()='Getting Started in Science 6']")
        self.driver.find_element(*LearningPage.chapter3).click()
        cp = CoursePage(self.driver)
        return cp

    def waitForElementVisibility_Xpath(self, xpath):
        WebDriverWait(self.driver,30).until(
        EC.presence_of_element_located((By.XPATH, xpath)))

    #########################################################################

    #Testing
    @allure.step("Entered search value in searchbar")
    def enter_searchValue(self):

        self.send_value(self.searchBar,"Algebra")

    #TEsting
    @allure.step("Selected search result")
    def select_searchResult(self):

       # self.click(self.goToCourse_Button)
       self.click(self.goToCourse_Button)

    def select_searchResult1(self):

        # self.click(self.goToCourse_Button)
        self.click(self.goToCourse_Button)


    #Testing
    @allure.step("Clicked on Course Outline button")
    def click_courseOutline_button(self):

        self.click(self.courseOutline_Button)
       # self.click(self.liveClass_button)


    #Testing
    @allure.step("Clicked on Unit")
    def click_unit(self):
        self.waitForElementVisibility_Xpath("//div[@class='rst__tree c-course-outline']/div")
        aList = self.driver.find_elements_by_xpath("//div[@class='rst__tree c-course-outline']/div")
        for a in aList:
            a.find_element_by_xpath("div[2]/div/div/div[1]/button").click()
            # a.find_element_by_xpath("[2]").click()
            break
        time.sleep(5)

    def click_unit_student(self, unitname):
        before_unit = "//label[text()='"
        after_unit = "']/parent::div/parent::div/parent::div/preceding-sibling::div[2]/button/i"
        unit_click = self.driver.find_element_by_xpath(before_unit + unitname + after_unit)
        unit_click.click()

    #TEsting
    @allure.step("Clicked on Chapter")
    def click_chapter(self):

        self.click(self.chapter3)
        cp = CoursePage(self.driver)
        return cp

    def click_chapter1(self):

        self.click(self.chapter5)
        self.click(self.chapter4)
        cp = CoursePage(self.driver)
        return cp

    def click_progress_button(self):
        self.waitForElementVisibility_Xpath("//a[text()='Progress']")
       # time.sleep(10)
        self.click(self.progress_Button)

    def click_unit_chapter(self):

        self.click(self.unit_Student)
        self.click(self.chapter_Student)
        cp = CoursePage(self.driver)
        return cp

    def click_unit_chapter1(self):


        self.click(self.unit_Student)
        self.click(self.topic_Student1)
        self.click(self.chapter4)
        cp = CoursePage(self.driver)
        return cp

    @allure.step("Entered search value in searchbar")
    def enter_value_in_searchbar(self):
        time.sleep(5)
        self.waitForElementVisibility_Xpath("//div[@class='o-input']/input")
        self.click(self.mycourses_dropdown)
        self.click(self.allcourses_dropdown)
        self.send_value(self.searchBar,"Chemistry")


    def select_search_result(self):
        time.sleep(10)

       # self.waitForElementVisibility_Xpath("(//button[text()='Go to Course'])[3]")

        #self.driver.find_element(*LearningPage.goToCourse_Button1).click()
        self.click(self.goToCourse_Button1)

    def select_search_result1(self):
        time.sleep(10)
        self.click(self.goToCourse_Button)


    def enter_value_in_searchBar(self):
        self.waitForElementVisibility_Xpath("//div[@class='o-input']/input")
        # time.sleep(10)
        self.driver.find_element(*LearningPage.searchBar).send_keys("Chemistry")

    def enter_value_in_searchBar1(self):
        self.waitForElementVisibility_Xpath("//div[@class='o-input']/input")
        # time.sleep(10)
        self.driver.find_element(*LearningPage.searchBar).send_keys(TestData.COURSE_NAME)