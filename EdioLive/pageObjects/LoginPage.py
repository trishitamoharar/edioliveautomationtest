import time

import allure
from selenium import webdriver

from selenium.webdriver.common.by import By

from EdioLive.pageObjects.DashboardPage import DashboardPage
from EdioLive.utilities.BaseClass import BaseClass
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class LoginPage(BaseClass):

    log=None

    def __init__(self, driver):
        self.driver=driver


    #global driver2
    userName=(By.ID,"username")
    password=(By.ID, "password")
    loginButton=(By.CLASS_NAME, "c-button")

    userName_Css=(By.CSS_SELECTOR, "#username")
    password_Css = (By.CSS_SELECTOR, "#password")
    login_ButtonCSS = (By.CSS_SELECTOR, ".c-button")
    username_trial=(By.XPATH, "(//*[@class='c-form-field'])[1]/div/div/input")


####################################################################################
    @allure.step("Entered login credentials")
    def enterLoginCredentials(self):

     #   time.sleep(20)
        WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, "username")))
        self.highLight("//input[@id='username']")
        self.driver.find_element(*LoginPage.userName).send_keys("ashissler")


        self.highLight("//input[@id='password']")
        self.driver.find_element(*LoginPage.password).send_keys("Passw0rd!")


        self.highLight("//button[@class='c-button']")
        self.driver.find_element(*LoginPage.loginButton).click()
        dash = DashboardPage(self.driver)
        return dash

    #TEsting to fail scripts
    def enterLoginCredentials1(self):
        log = self.getLogger()
        #   time.sleep(20)
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, "username1")))
        #  self.highLight("//input[@id='username']")
        self.driver.find_element(*LoginPage.userName).send_keys("ashissler")
        log.info("Entered login credentials")

        #   self.highLight("//input[@id='password']")
        self.driver.find_element(*LoginPage.password).send_keys("Passw0rd!")
        log.info("Entered password")

        #    self.highLight("//button[@class='c-button']")
        self.driver.find_element(*LoginPage.loginButton).click()
        dash = DashboardPage(self.driver)
        return dash

    @allure.step("Successfully logged into EDIO as a Student")
    def enterLoginCredentials_Student(self):
        log = self.getLogger()
        #time.sleep(30)
        #WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, "username")))
        #self.highLight("//input[@id='username']")

        self.waitForElementVisibility_Xpath("//input[@id='username']")
        self.driver.find_element(*LoginPage.userName).send_keys("BARSU")
        log.info("Entered username")
        #self.highLight("//input[@id='password']")
        self.driver.find_element(*LoginPage.password).send_keys("Passw0rd!")
        log.info("Entered password")
        #self.highLight("//button[@class='c-button']")
        self.driver.find_element(*LoginPage.loginButton).click()
        dash = DashboardPage(self.driver)
        log.info("Successfully logged into the application")
        return dash

    @allure.step("Successfully logged into EDIO as a Student")
    def enterLoginCredentials_Student(self):
        log = self.getLogger()
        # time.sleep(30)
        # WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, "username")))
        # self.highLight("//input[@id='username']")

        self.waitForElementVisibility_Xpath("//input[@id='username']")
        self.driver.find_element(*LoginPage.userName).send_keys("ANGLE")
        log.info("Entered username")
        # self.highLight("//input[@id='password']")
        self.driver.find_element(*LoginPage.password).send_keys("Passw0rd!")
        log.info("Entered password")
        # self.highLight("//button[@class='c-button']")
        self.driver.find_element(*LoginPage.loginButton).click()
        dash = DashboardPage(self.driver)
        log.info("Successfully logged into the application")
        return dash

    #Testing try catch
    def enterLoginCredentials1(self):
        log = self.getLogger()
        try:
            #WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, "username")))
            self.driver.find_element(*LoginPage.userName1).send_keys("ashissler")
            self.driver.find_element(*LoginPage.password).send_keys("Passw0rd!")
            # self.verifyLinkPresence_ByWaiting(self.loginButton)
            self.driver.find_element(*LoginPage.loginButton).click()
            dash = DashboardPage(self.driver)
            return dash
        except Exception as e:
            log.error("Login not successful",e)
            print(e)

    def launch(self):
        global driver2
        driver2 = webdriver.Firefox(executable_path="D:\\Workspace\\geckodriver-v0.29.0-win64\\geckodriver.exe")
        driver2.get("https://demo-edio.ccaeducate.me/")
       # child = self.driver2.window_handle
       # print(child)
       # self.driver2.switch_to.window(child)


    def waitForElementVisibility_Xpath(self, xpath):
        WebDriverWait(self.driver,30).until(
        EC.presence_of_element_located((By.XPATH, xpath)))

    def enter_credentials(self, username, password):
        print(username)
        print(password)
        self.send_value(self.userName, username)
        self.send_value(self.password, password)
        self.click(self.loginButton)
        dash = DashboardPage(self.driver)
        return dash
