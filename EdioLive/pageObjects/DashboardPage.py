import time

import allure
from selenium.webdriver.common.by import By

from EdioLive.pageObjects.CoursePage import CoursePage
from EdioLive.pageObjects.LearningPage import LearningPage
from EdioLive.utilities.BaseClass import BaseClass
from selenium.webdriver.common.keys import Keys


class DashboardPage(BaseClass):

    def __init__(self, driver):
        self.driver=driver


    system_Button=(By.XPATH, "//span[text()='Webmail']")
    learningButton=(By.XPATH, "//span[text()='Learning']")
    useAnother_Button=(By.XPATH, "//div[@class='inline-block']/input")
    name=(By.XPATH, "//input[@name='loginfmt']")



    def getDefaultWindowHandle(self):
        # get current window handle
        p = self.driver.current_window_handle
        return p

    def clickSystemButton(self):
        self.waitForElementVisibility_Xpath("//span[text()='Webmail']")
        # get current window handle
        p = self.driver.current_window_handle
        self.driver.find_element(*DashboardPage.system_Button).click()

        child = self.driver.window_handles

        for w in child:
            # switch focus to child window
            if (w != p):
                self.driver.switch_to.window(w)
                print("New Window:" + w)
                break
        time.sleep(10)
        self.driver.find_element(*DashboardPage.name).send_keys("mahesh")
        self.driver.find_element(*DashboardPage.useAnother_Button).click()





    def enterClassroom4(self):

        lp = self.clickLearningButton()
        lp.enterValueInSearchBar2()
        lp.selectSearchResult1()
        lp.clickCourseOutlineButton()
        lp.clickUnit1()
        cp = lp.clickChapter2()
        cp.clickVideoLink()
        jit=cp.clickLaunchButton1()
        return jit



    def enterClassroom_Student(self):

        log = self.getLogger()

        lp = self.click_learning_button()
        # lp.enter_searchValue()
        log.info("Clicked on learning button")

        lp.enter_value_in_searchBar1()
        log.info("Entered a search Value")
        time.sleep(4)
        # lp.select_searchResult()
        lp.select_search_result1()
        time.sleep(4)
        lp.click_progress_button()
        time.sleep(5)
        lp.click_unit_student("Introduction to Chemistry & Scientific Measurement")
        time.sleep(5)
        cp = lp.click_chapter()
        cp.click_videoLink()
        jit = cp.click_launch_button()
        return jit
##########################################################################

    #Testing
    def enter_classroom(self):

        lp = self.click_learning_button()
       # lp.enter_searchValue()
        lp.enter_value_in_searchbar()
        #lp.select_searchResult()
        lp.select_search_result()
        lp.click_courseOutline_button()
        lp.click_unit()
        cp = lp.click_chapter()
        cp.click_videoLink()
        jit = cp.click_launch_button()
        return jit

    def enter_classroom1(self):
        log=self.getLogger()

        lp = self.click_learning_button()
       # lp.enter_searchValue()
        log.info("Clicked on learning button")

        lp.enter_value_in_searchBar1()
        log.info("Entered a search Value")
        #lp.select_searchResult()
        lp.select_search_result1()
        lp.click_courseOutline_button()
        lp.click_unit()
        cp = lp.click_chapter()
        cp.click_videoLink()
        jit = cp.click_launch_button()
        return jit

    def enter_classroom2(self):

        lp = self.click_learning_button()
       # lp.enter_searchValue()
        lp.enter_value_in_searchBar1()
        #lp.select_searchResult()
        lp.select_search_result1()
        lp.click_progress_button()
        cp=lp.click_unit_chapter1()
        cp.click_videoLink()
        jit = cp.click_launch_button()
        return jit

    @allure.step("Clicked on Learning button")
    def click_learning_button(self):

        self.click(self.learningButton)
        lp = LearningPage(self.driver)
        return lp

    def click_chattab_teacher(self,stuname):
        self.driver.find_element_by_xpath("//*[@class='c-navigation-wrapper']/ul/li[8]/div/a").click()
        time.sleep(10)
        self.driver.find_element_by_xpath("//input[@placeholder='Search Active Conversations']").send_keys(stuname)
        time.sleep(10)
        self.driver.find_element_by_xpath("//li[@class='c-chat__list__item']").click()
        time.sleep(10)
        self.driver.find_element_by_xpath("//textarea[@class='c-chat-window__input__text ']").send_keys("Hello Student",Keys.ENTER)

    def click_chattab_student(self):
        self.driver.find_element_by_xpath("//*[@class='c-navigation-wrapper']/ul/li[7]/div/a").click()
        time.sleep(10)
        countmesg = int(self.driver.find_element_by_xpath("//li[@class='c-chat__list__item']/div[3]/span").text)
        assert countmesg == 1




