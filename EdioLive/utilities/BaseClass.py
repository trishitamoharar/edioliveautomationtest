import inspect
import logging
import time

import pytest
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BaseClass:
    driver = None




    def getChrome(self):
        global driver
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        self.driver = webdriver.Chrome(executable_path="D:\\Workspace\\chromedriver_win32 (1)\\chromedriver.exe",
                                  chrome_options=chrome_options)
        self.driver.get("https://demo-edio.ccaeducate.me/")
        self.driver.maximize_window()
        self.driver.implicitly_wait(120)
        return self.driver

    def getChrome1(self):
        global driver
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--incognito")
        self.driver = webdriver.Chrome(executable_path="D:\\Workspace\\chromedriver_win32 (1)\\chromedriver.exe",
                                  chrome_options=chrome_options)
        self.driver.get("https://demo-edio.ccaeducate.me/")
        self.driver.maximize_window()
        self.driver.implicitly_wait(120)
        return self.driver

    def pytest_addoption(parser):
        parser.addoption("--browser_name", action="store", default="chrome")

    #Explicit wait by Xpath
    def waitForElementVisibility_Xpath(self, xpath):
        WebDriverWait(self.driver,30).until(
        EC.presence_of_element_located((By.XPATH, xpath)))


    def getLogger(self):
        loggerName = inspect.stack()[1][3]
        logger = logging.getLogger(loggerName)
        fileHandler = logging.FileHandler('logfile.log')
        formatter = logging.Formatter("%(asctime)s :%(levelname)s : %(name)s :%(message)s")
        fileHandler.setFormatter(formatter)

        logger.addHandler(fileHandler)  # filehandler object

        logger.setLevel(logging.DEBUG)
        return logger





    def clickByJavaScript(self, xpath):
        element=self.driver.find_element(By.XPATH, xpath)
        self.driver.execute_script("arguments[0].click();", element);

    def switchToNewTab(self, p):

        # get first child window
        child = self.driver.window_handles

        for w in child:
            # switch focus to child window
            if (w != p):
                self.driver.switch_to.window(w)
                print("New Window:" + w)
                break


    def getDefaultWindowHandle(self):
        # get current window handle
        p = self.driver.current_window_handle
        return p

    def highLight(self, xpath):
        element=self.driver.find_element(By.XPATH, xpath)
        self.driver.execute_script("arguments[0].setAttribute('style', arguments[1]);", element,
                         "border: 3px solid red;")
        time.sleep(0.5)
        self.driver.execute_script("arguments[0].setAttribute('style', arguments[1]);", element, "border:'';")

    def send_value(self, by_locator, text):
      #  self.highLight(by_locator)
        WebDriverWait(self.driver, 30).until(EC.visibility_of_element_located(by_locator)).send_keys(text)

    def click(self, by_locator):
     #   self.highLight(by_locator)
        WebDriverWait(self.driver, 30).until(EC.visibility_of_element_located(by_locator)).click()

    def is_visible(self, by_locator):
        element = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator))
        if element.is_displayed():
            print("Visible")
        else:
            print("NOt visible")


    def hover(self):

        element = driver.find_element_by_xpath("(//div[@class='participant__profile'])[1]")
        hoverMethod = ActionChains(self.driver).move_to_element(element)
        hoverMethod.perform()
