import os


class TestData:
    path = os.path.dirname(os.getcwd())
    CHROME_PATH = path+"\\EdioLive\\EdioLive\\Drivers\\chromedriver.exe"
    URL = "https://demo-edio.ccaeducate.me/"
    TEACHER_USERNAME="ashissler"
    TEACHER_PASSWORD="Passw0rd!"

    TITLE_Page="Edio | Dashboard"

    STUDENT_USERNAME="BARSU"
    STUDENT_PASSWORD="Passw0rd!"

    STUDENT_USERNAME1 = "ABROM092"
    STUDENT_PASSWORD1 = "Passw0rd!"

    STUDENT_USERNAME2 = "AMARO938"

    TEACHER_USERNAME2="aspearmankannel"
    TEACHER_USERNAME1 = "cvoitek"
    STUDENT_USERNAME3="ACKER577"
    STUDENT_USERNAME4 = "ALDON915"
    COURSE_NAME2="English 11 American Literature"
    COURSE_NAME1 = "Algebra 1A"
    COURSE_NAME = "Chemistry"
    SEARCHNAME_FIRSTNAME_STUDENT="MACY"
    SEARCHNAME_LASTNAME_STUDENT = "ABROMITIS"
    SEARCHNAME_PARTIALVALUE_STUDENT = "CKE"
    SEARCHNAME_PARTIALVALUE_STUDENT1 = "ABR"

    OBJECTIONABLE_CONTENT_1="XXX"
    OBJECTIONABLE_CONTENT_2 = "xxx"
    OBJECTIONABLE_CONTENT_3 = "ACXXXDD"
