import logging

import allure
from selenium import webdriver
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.support.events import EventFiringWebDriver, AbstractEventListener
from selenium.webdriver.support.wait import WebDriverWait as EC, WebDriverWait


class MyListener(AbstractEventListener):
    def before_navigate_to(self, url, driver):
        driver.delete_all_cookies()
        print("Before navigating to ", url)

    def after_navigate_to(self, url, driver):
        print("After navigating to ", url)

    def before_navigate_back(self, driver):
        print("before navigating back ", driver.current_url)

    def after_navigate_back(self, driver):
        print("After navigating back ", driver.current_url)

    @allure.step("Navigated to")
    def before_navigate_forward(self, driver):
        print("before navigating forward ", driver.current_url)

    def after_navigate_forward(self, driver):
        print("After navigating forward ", driver.current_url)

    def before_find(self, by, value, driver):
        print("before find")

    def after_find(self, by, value, driver):
     #   print("after_find")
        pass

    def before_click(self, element, driver):
      #  print("before_click")

      value=element.is_displayed()

    @allure.step("Clicked on element")
    def after_click(self, element, driver):
        print("Clicked on" +element.text)
        pass


    def before_change_value_of(self, element, driver):
        print("before_change_value_of")
        element.clear()

    def after_change_value_of(self, element, driver):
        print("after_change_value_of")

    def before_execute_script(self, script, driver):
        print("before_execute_script")

    def after_execute_script(self, script, driver):
        print("after_execute_script")

    def before_close(self, driver):
        print("tttt")

    def after_close(self, driver):
        print("before_close")

    def before_quit(self, driver):
        print("before_quit")

    def after_quit(self, driver):
        print("after_quit")
        driver.close()
        driver.quit()

    def on_exception(self, exception, driver):
        allure.attach(driver.get_screenshot_as_png(),
                      name="Invalid", attachment_type=allure.attachment_type.PNG)